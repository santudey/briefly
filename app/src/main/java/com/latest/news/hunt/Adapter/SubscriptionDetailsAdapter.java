package com.latest.news.hunt.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Activity.DashboardActivity;
import com.latest.news.hunt.Activity.PublisherDetailsActivity;
import com.latest.news.hunt.Fragment.SubscriptionFragment;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.CommonUtility;
import com.latest.news.hunt.Util.ConnectionDetector;
import com.latest.news.hunt.Util.GlobalInterface;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.Result;
import com.latest.news.hunt.WebService.Pojo.Subscribelist;
import com.latest.news.hunt.WebService.Rest_Client;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 30/5/16.
 */
public class SubscriptionDetailsAdapter extends RecyclerView.Adapter< RecyclerView.ViewHolder > {
    List< Result > feedList;
    private Context context;
    private static final int ITEM_VIEW_TYPE_NORMAL = 0;
    private static final int ITEM_VIEW_TYPE_FOOTER = 1;
    ConnectionDetector connectionDetector;
    boolean flagSubscribeClicked = false;
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;
    PrefsManager prefsManager;
    SubscriptionFragment fragment;

    public SubscriptionDetailsAdapter( List< Result > feedList, Context context, SubscriptionFragment fragment ) {
        prefsManager = new PrefsManager( context );
        this.feedList = feedList;
        this.context = context;
        connectionDetector = new ConnectionDetector( context );
        fontRegular = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Regular.ttf");
        fontBold = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Bold.ttf");
        fontMedium = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Medium.ttf");
        this.fragment = fragment;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        if( viewType == ITEM_VIEW_TYPE_NORMAL )
            return new SubscriptionDetailsAdapter.ViewHolderNoraml( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.subscribe_normal_row, parent, false ) );
        else
            return new SubscriptionDetailsAdapter.ViewHolderFooter( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.news_details_footer_row, parent, false ) );
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, final int position ) {
        if( getItemViewType( position ) == ITEM_VIEW_TYPE_NORMAL ) {
            final ViewHolderNoraml viewHolderNoraml = ( ViewHolderNoraml ) holder;
            flagSubscribeClicked = false;
            if( feedList.get( position ).getType( ).equals( "1" ) ) {
                viewHolderNoraml.rldRequestSite.setVisibility( View.GONE );
                viewHolderNoraml.rlNormalRow.setVisibility( View.VISIBLE );
                viewHolderNoraml.tvHeader.setVisibility( View.GONE );
                Uri publisherLogouri = Uri.parse( feedList.get( position ).getPublisher_logo( ) );
                viewHolderNoraml.sdvPublisherLogo.setImageURI( publisherLogouri );
                viewHolderNoraml.tvNoOfPeopleSubscribe.setText( feedList.get( position ).getNo_of_people_subscribe( ));
                viewHolderNoraml.tvPublisherName.setText( feedList.get( position ).getPublisher_alias_name( ) );
               if( feedList.get( position ).getSubscribe_status().equals( "true" ) ) {
                   viewHolderNoraml.ivSubscriptionStatus.setBackground( ContextCompat.getDrawable( context,R.drawable.check ) );
               } else {
                   viewHolderNoraml.ivSubscriptionStatus.setBackground( ContextCompat.getDrawable( context,R.drawable.plus ) );
               }
            } else if(feedList.get( position ).getType( ).equals( "2" )){
                viewHolderNoraml.rldRequestSite.setVisibility( View.GONE );
                viewHolderNoraml.rlNormalRow.setVisibility( View.GONE );
                viewHolderNoraml.tvHeader.setVisibility( View.VISIBLE );
                viewHolderNoraml.tvHeader.setText( feedList.get( position ).getHeader( ) );
            } else {
                viewHolderNoraml.rlNormalRow.setVisibility( View.GONE );
                viewHolderNoraml.tvHeader.setVisibility( View.GONE );
                viewHolderNoraml.rldRequestSite.setVisibility( View.VISIBLE );
                viewHolderNoraml.rldRequestSite.setOnClickListener( new View.OnClickListener( ) {
                    @Override
                    public void onClick( View v ) {
                        CommonUtility.shareBygmail( context, "Request to add a site", CommonUtility.getEmailId( context ),
                                "", context.getResources().getString( R.string.company_email ));
                    }
                } );
            }
            viewHolderNoraml.rlAdd.setOnClickListener( new View.OnClickListener( ) {
                @Override
                public void onClick( View v ) {
                    if(position == 1 && SubscriptionFragment.mySubscriptionSize == 1) {
                        Toast.makeText( context,"Need atleast One subscription",Toast.LENGTH_SHORT ).show();
                    } else {

                        AppEventsLogger.activateApp( context );
                        FirebaseAnalytics sublistClick = FirebaseAnalytics.getInstance( context );
                        Bundle bundlsublist = new Bundle( );
                        sublistClick.logEvent( "Subscription Add Click", bundlsublist );

                        if(!flagSubscribeClicked) {
                            flagSubscribeClicked = true;
                            doSubscribe( position,feedList.get( position ).getPublisher_name() );
                        }
                    }
                }
            } );
            viewHolderNoraml.itemView.setOnClickListener( new View.OnClickListener( ) {
                @Override
                public void onClick( View v ) {
                    if(feedList.get( position ).getType( ).equals( "1" )) {

                        AppEventsLogger.activateApp( context );
                        FirebaseAnalytics sublistClick = FirebaseAnalytics.getInstance( context );
                        Bundle bundlsublist = new Bundle( );
                        sublistClick.logEvent( "Subscription Pub Details Click", bundlsublist );

                        SubscriptionFragment.scrollPosition = position;
                        Intent intent = new Intent( context, PublisherDetailsActivity.class );
                        intent.putExtra( "publisherName", feedList.get( position ).getPublisher_name() );
                        context.startActivity( intent );
                    }
                }
            } );
        } else {
            final ViewHolderFooter viewHolderFooter = ( ViewHolderFooter ) holder;
            if( SubscriptionFragment.isLoad) {
                viewHolderFooter.rlFooter.setVisibility( View.VISIBLE );
                viewHolderFooter.progress_bar.setVisibility( View.VISIBLE );
                viewHolderFooter.tvNoMore.setVisibility( View.GONE );
            } else {
                viewHolderFooter.rlFooter.setVisibility( View.GONE );
                viewHolderFooter.progress_bar.setVisibility( View.GONE );
                viewHolderFooter.tvNoMore.setVisibility( View.GONE );
            }
        }
    }

    @Override
    public int getItemCount( ) {
        return feedList.size( ) + 1;
    }

    @Override
    public int getItemViewType( int position ) {
        if( position == getItemCount( ) - 1 ) {
            return ITEM_VIEW_TYPE_FOOTER;
        } else {
            return ITEM_VIEW_TYPE_NORMAL;
        }
    }

    public static class ViewHolderNoraml extends RecyclerView.ViewHolder {
        SimpleDraweeView sdvPublisherLogo;
        ImageView ivSubscriptionStatus;
        TextView tvHeader;
        TextView tvPublisherName;
        TextView tvNoOfPeopleSubscribe;
        RelativeLayout rlAdd;
        RelativeLayout rlNormalRow;
        RelativeLayout rldRequestSite;
        TextView tvRequestSite;
        public ViewHolderNoraml( View itemView ) {
            super( itemView );
            rldRequestSite = ( RelativeLayout ) itemView.findViewById( R.id.rldRequestSite );
            sdvPublisherLogo = ( SimpleDraweeView ) itemView.findViewById( R.id.sdvPublisherLogo );
            tvHeader = ( TextView ) itemView.findViewById( R.id.tvHeader );
            tvPublisherName = ( TextView ) itemView.findViewById( R.id.tvPublisherName );
            tvNoOfPeopleSubscribe = ( TextView ) itemView.findViewById( R.id.tvNoOfPeopleSubscribe );
            rlAdd = ( RelativeLayout ) itemView.findViewById( R.id.rlAdd );
            rlNormalRow = ( RelativeLayout ) itemView.findViewById( R.id.rlNormalRow );
            ivSubscriptionStatus = ( ImageView ) itemView.findViewById( R.id.ivSubscriptionStatus );
            tvRequestSite = ( TextView ) itemView.findViewById( R.id.tvRequestSite );
            tvHeader.setTypeface( fontRegular );
            tvPublisherName.setTypeface( fontRegular );
            tvNoOfPeopleSubscribe.setTypeface( fontRegular );
            tvRequestSite.setTypeface( fontRegular );
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {
        TextView tvNoMore;
        ProgressWheel progress_bar;
        RelativeLayout rlFooter;

        public ViewHolderFooter( View itemView ) {
            super( itemView );
            rlFooter = ( RelativeLayout ) itemView.findViewById( R.id.rlFooter );
            progress_bar = ( ProgressWheel ) itemView.findViewById( R.id.progress_bar );
            tvNoMore = ( TextView ) itemView.findViewById( R.id.tvNoMore );
            tvNoMore.setTypeface( fontRegular );
            tvNoMore.setText( "No More Publisher" );
        }
    }

    private void doSubscribe( final int pos, String publisherName ) {
        try {
            Rest_Client.get( ).doSubscribe( prefsManager.getAccessToken(), publisherName, new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    if( mainPojo.getStatus( ).equals( "Success" ) ) {
                        if(mainPojo.getResponse().equals( "Sucessfully subscribed" )) {
                            feedList.get( pos ).setSubscribe_status( "true" );
                        } else {
                            feedList.get( pos ).setSubscribe_status( "false" );
                        }
                        notifyDataSetChanged( );
                        ((GlobalInterface)fragment).getLoad( 1 );
                    } else {
                        flagSubscribeClicked = false;
                    }
                }

                @Override
                public void failure( RetrofitError error ) {
                    flagSubscribeClicked = false;
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }
}
