package com.latest.news.hunt.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Window;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.photodraweeview.PhotoDraweeView;

/**
 * Created by sushovan on 2/4/16.
 */
public class DisplayLargeImageActivity  extends Activity {

    private String url;
    private Uri imageUri;
    private Dialog progressBarDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActionBar().hide();
        showProgressDialog();
        url = getIntent().getStringExtra("imageUrl");
        final PhotoDraweeView imageView = new PhotoDraweeView(this);
        imageView.setBackground( ContextCompat.getDrawable( this, R.drawable.black_back ));
        setContentView(imageView);
        imageUri = Uri.parse(url);
        PipelineDraweeControllerBuilder controller = Fresco.newDraweeControllerBuilder( );
        controller.setUri(imageUri);

        controller.setOldController(imageView.getController());
        controller.setControllerListener(new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                if (imageInfo == null || imageView == null) {
                    return;
                }
                imageView.update(imageInfo.getWidth(), imageInfo.getHeight());
                dismissProgressDialog();
            }
        });

        imageView.setController(controller.build());
    }

    @Override
    protected void onResume() {
        super.onResume();
//        ApplicationGlobal.sAccessToken = new PrefsManager(this).getToken();
    }

    private void showProgressDialog() {
        progressBarDialog = new Dialog(this);
        progressBarDialog.getWindow().requestFeature( Window.FEATURE_NO_TITLE);
        progressBarDialog.setContentView(R.layout.progress_dialog);
        progressBarDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressBarDialog.setCancelable(false);
        progressBarDialog.show();
    }


    private void dismissProgressDialog() {
        if (progressBarDialog != null)
            if (progressBarDialog.isShowing())
                progressBarDialog.dismiss();
        progressBarDialog = null;
    }

}
