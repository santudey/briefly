package com.latest.news.hunt.Activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Adapter.DashboardPagerAdapter;
import com.latest.news.hunt.BuildConfig;
import com.latest.news.hunt.Fragment.AppupdateDialogFragment;
import com.latest.news.hunt.Fragment.FeedFragment;
import com.latest.news.hunt.Fragment.SavedFragment;
import com.latest.news.hunt.Fragment.TrendingFragment;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.AndroidRating.AppRate;
import com.latest.news.hunt.Util.AndroidRating.OnClickButtonListener;
import com.latest.news.hunt.Util.CommonUtility;
import com.latest.news.hunt.Util.ConnectionDetectorForActivity;
import com.latest.news.hunt.Util.Constants;
import com.latest.news.hunt.Util.CustomTabLayout;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.Util.ViewPageTransformer;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.RegisterPojo;
import com.latest.news.hunt.WebService.Rest_Client;

import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 14/5/16.
 */
public class DashboardActivity extends AppCompatActivity {
    Toolbar toolbar;
    CustomTabLayout tabs;
    ViewPager pager;
    TextView tvTollbarTitle;
    TextView tvInvite;
    TextView noDataTitle;
    TextView noDataSubTitle;
    TextView noDataTv;
    RelativeLayout rlFacebookInvite;
    RelativeLayout rlProgressWheel;
    RelativeLayout noDataOverlay;
    ImageView ivInternet;
    DashboardPagerAdapter adapter;
    private int Numboftabs = 4;
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;
    PrefsManager prefsManager;
    private GoogleCloudMessaging gcm;
    private String regId;
    ConnectionDetectorForActivity connectionDetector;
    private String app_versioncode = "";
    private String noti_push = "";
    private FirebaseAnalytics mFirebaseAnalytics;
    private static final int PERMISSION_REQUEST_CODE = 1;
    String email = "";

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        mFirebaseAnalytics.logEvent("DashboardActivity", params);
        initTheme( );
        initFonts( );
        initViews( );
        if( getIntent( ).hasExtra( "noti_push" ) ) {
            noti_push = getIntent( ).getStringExtra( "noti_push" );
        }
        initToolbar( );
        initTablayout( );
        initViewpager( );
        prefsManager = new PrefsManager( this );
        if( prefsManager.getGcmRegId( ).equals( "" ) ) {
            if(connectionDetector.isConnectingToInternet()) {
                registerGCM( );
            } else {
                noDataOverlay.setVisibility( View.VISIBLE );
            }
        } else {
            pager.setAdapter( adapter );
        }
        rlFacebookInvite.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                Bundle params = new Bundle();
                mFirebaseAnalytics.logEvent("Facebook App Invite", params);
                fbInvite( );
            }
        } );
        ivInternet.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                CommonUtility.reRegistercall = false;
                setUpRegId();
            }
        } );
        appRate();
    }

    private void initFonts( ) {
        fontRegular = Typeface.createFromAsset( getAssets( ), "fonts/Roboto-Regular.ttf" );
        fontBold = Typeface.createFromAsset( getAssets( ), "fonts/Roboto-Bold.ttf" );
        fontMedium = Typeface.createFromAsset( getAssets( ), "fonts/Roboto-Medium.ttf" );
    }

    private void initViewpager( ) {
        adapter = new DashboardPagerAdapter( getSupportFragmentManager( ), Numboftabs );
        pager.setOffscreenPageLimit( 1 );
        pager.setPageTransformer( true, new ViewPageTransformer( ViewPageTransformer.TransformType.FADE ) );
        pager.addOnPageChangeListener( new TabLayout.TabLayoutOnPageChangeListener( tabs ) );
        tabs.setOnTabSelectedListener( new TabLayout.OnTabSelectedListener( ) {
            @Override
            public void onTabSelected( TabLayout.Tab tab ) {
                pager.setCurrentItem( tab.getPosition( ) );
            }

            @Override
            public void onTabUnselected( TabLayout.Tab tab ) {

            }

            @Override
            public void onTabReselected( TabLayout.Tab tab ) {

            }
        } );
    }


    @Override
    protected void onPause( ) {
        super.onPause( );
        CommonUtility.feedIsResumed = false;
        CommonUtility.trendingIsResumed = false;
        CommonUtility.savedIsResumed = false;
    }

    private void initTablayout( ) {
        tabs.addTab( tabs.newTab( ).setText( "Feed" ) );
        tabs.addTab( tabs.newTab( ).setText( "Trending" ) );
        tabs.addTab( tabs.newTab( ).setText( "Saved" ) );
        tabs.addTab( tabs.newTab( ).setText( "Subscription" ) );
    }

    private void initToolbar( ) {
        setSupportActionBar( toolbar );
        if( getSupportActionBar( ) != null ) {
            getSupportActionBar( ).setDisplayHomeAsUpEnabled( false );
            getSupportActionBar( ).setDisplayShowHomeEnabled( false );
            getSupportActionBar( ).setTitle( "" );
            tvTollbarTitle.setTypeface( fontBold );
            toolbar.setTitleTextColor( getResources( ).getColor( R.color.black ) );
            tvInvite.setTypeface( fontRegular );
        }
    }

    private void initTheme( ) {
        setTheme( R.style.AppTheme );
        if( Build.VERSION.SDK_INT >= 21 ) {
            getWindow( ).setNavigationBarColor( getResources( ).getColor( R.color.blue_news10 ) );
            getWindow( ).setStatusBarColor( getResources( ).getColor( R.color.blue_news10 ) );
        }
    }

    private void initViews( ) {
        connectionDetector = new ConnectionDetectorForActivity( this );
        setContentView( R.layout.activity_dashboard );
        toolbar = ( Toolbar ) findViewById( R.id.toolbar );
        pager = ( ViewPager ) findViewById( R.id.pager );
        tabs = ( CustomTabLayout ) findViewById( R.id.tabs );
        tvTollbarTitle = ( TextView ) findViewById( R.id.tvTollbarTitle );
        tvInvite = ( TextView ) findViewById( R.id.tvInvite );
        noDataTv = ( TextView ) findViewById( R.id.noDataTv );
        noDataSubTitle = ( TextView ) findViewById( R.id.noDataSubTitle );
        noDataTitle = ( TextView ) findViewById( R.id.noDataTitle );
        rlFacebookInvite = ( RelativeLayout ) findViewById( R.id.rlFacebookInvite );
        noDataOverlay = ( RelativeLayout ) findViewById( R.id.noDataOverlay );
        rlProgressWheel = ( RelativeLayout ) findViewById( R.id.rlProgressWheel );
        ivInternet = ( ImageView ) findViewById( R.id.ivInternet );
        tvInvite.setTypeface( fontRegular );
        noDataTv.setTypeface( fontRegular );
        noDataSubTitle.setTypeface( fontRegular );
    }

    public void registerGCM( ) {
        gcm = GoogleCloudMessaging.getInstance( DashboardActivity.this );
        regId = prefsManager.getGcmRegId( );
        if( TextUtils.isEmpty( regId ) ) {
            registerInBackground( );
            Log.d( "RegisterActivity", "registerGCM - successfully registered with GCM server - regId: " + regId );
        } else {
            if( !prefsManager.isRegIdDelivered( ) )
                CommonUtility.reRegistercall = false;
                setUpRegId( );
        }
    }

    private void registerInBackground( ) {
        new AsyncTask( ) {
            @Override
            protected void onPostExecute( Object msg ) {
                super.onPostExecute( msg );
            }

            @Override
            protected Object doInBackground( Object[] params ) {
                String msg = "";
                try {
                    if( gcm == null ) {
                        gcm = GoogleCloudMessaging.getInstance( DashboardActivity.this );
                    }
                    regId = gcm.register( Constants.GCM_PROJECT_NO );
                    msg = "Device registered, registration ID=" + regId;
                    prefsManager.saveGcmRegId( regId );
                    CommonUtility.reRegistercall = false;
                    setUpRegId( );
                } catch( IOException ex ) {
                    msg = "Error :" + ex.getMessage( );
                }
                Log.d( "RegisterActivity", "AsyncTask completed: " + msg );
                return msg;
            }
        }.execute( null, null, null );
    }

    public void setUpRegId( ) {
//        if (!checkPermission()) {
//            requestPermission();
//        } else {
//            email = CommonUtility.getEmailId( this );
//        }

            try {
                try {
                    rlProgressWheel.setVisibility( View.VISIBLE );
                } catch( Exception e ) {}
                Rest_Client.get( ).userRegister( CommonUtility.getEmailId( this ),
                        getResources( ).getString( R.string.app_name ),
                        CommonUtility.getCountry( DashboardActivity.this ),
                        regId, new Callback< MainPojo >( ) {
                    @Override
                    public void success( MainPojo mainPojo, Response response ) {
                           noDataOverlay.setVisibility( View.GONE );
                           rlProgressWheel.setVisibility( View.GONE );
                           if( mainPojo.getResponse( ).equals( "Success" ) ) {
                               prefsManager.setAccessToken( mainPojo.getAuthkey( ) );
                               if( !CommonUtility.reRegistercall ) {
                                   pager.setAdapter( adapter );
                               }
                               app_versioncode = mainPojo.getApi_version( ).toString( );
                               prefsManager.setcurrentAppVersion( app_versioncode );
                           }
                    }

                    @Override
                    public void failure( RetrofitError error ) {
                        rlProgressWheel.setVisibility( View.GONE );
                        noDataOverlay.setVisibility( View.VISIBLE );
                        connectionDetector.showRetrofitErrorToast( error );
                    }
                } );
            } catch( NullPointerException e ) {
                e.printStackTrace( );
            }
    }

    public void doAppUpdate( String app_versioncode ) {
        if( BuildConfig.VERSION_CODE < Integer.parseInt( app_versioncode ) ) {
            int appUpdateCount = Integer.parseInt( prefsManager.getApptUpdateCounter( ) );
            if( ( ++appUpdateCount ) % 5 == 0 ) {
                FragmentManager fm = getSupportFragmentManager( );
                AppupdateDialogFragment fragment = new AppupdateDialogFragment( );
                fragment.show( fm, "" );
            }
            prefsManager.setApptUpdateCounter( String.valueOf( ++appUpdateCount ) );
        }
    }

    private void fbInvite( ) {
        String appLinkUrl, previewImageUrl;
        appLinkUrl = "https://fb.me/546938022179694";
        previewImageUrl = "https://s6.postimg.org/7l1e0pcup/playstore_icon.png";

        if ( AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);
        } else {
            Toast.makeText( this, "Please install Facebook", Toast.LENGTH_SHORT ).show();
        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    email = CommonUtility.getEmailId( this );
//                } else {
//                    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.GET_ACCOUNTS},PERMISSION_REQUEST_CODE);
//                }
//                break;
//        }
//    }
//
//    private boolean checkPermission(){
//        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
//        if (result == PackageManager.PERMISSION_GRANTED){
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    private void requestPermission(){
//        if ( ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.GET_ACCOUNTS)){
//        } else {
//            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.GET_ACCOUNTS},PERMISSION_REQUEST_CODE);
//        }
//    }

    private void appRate() {

        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.rating_dialog,
                (ViewGroup )findViewById(R.id.main_content), false);

        AppRate.with(this)
                .setInstallDays(3) // default 10, 0 means install day. // 3
                .setLaunchTimes(3) // default 10 // 5
                .setRemindInterval(5) // default 1
                .setShowLaterButton(true) // default true
                .setView(customView)
                .setCancelable(true)
                .setShowTitle(false)
//                    .setDebug(true) // this always displays the pop-up message.
                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {
                        switch( which ) {
                            case -1:
                                // Build and send an Event.
                                AppEventsLogger.activateApp(DashboardActivity.this);
                                FirebaseAnalytics snalyticsRate = FirebaseAnalytics.getInstance(DashboardActivity.this);
                                Bundle paramsrate = new Bundle();
                                snalyticsRate.logEvent("AppRate Clicked", paramsrate);
                                break;
                            case -2:
                                // Build and send an Event.
                                AppEventsLogger.activateApp(DashboardActivity.this);
                                FirebaseAnalytics snalyticsRatelater = FirebaseAnalytics.getInstance(DashboardActivity.this);
                                Bundle paramsratelater = new Bundle();
                                snalyticsRatelater.logEvent("Rate Later Clicked", paramsratelater);
                                break;
                            case -3:
                                // Build and send an Event.
                                AppEventsLogger.activateApp(DashboardActivity.this);
                                FirebaseAnalytics snalyticsRatecancel = FirebaseAnalytics.getInstance(DashboardActivity.this);
                                Bundle paramsratecancel = new Bundle();
                                snalyticsRatecancel.logEvent("Rate cancel Clicked", paramsratecancel);
                                break;
                            case -4:
                                // Build and send an Event.
                                AppEventsLogger.activateApp(DashboardActivity.this);
                                FirebaseAnalytics snalyticsRateoutside = FirebaseAnalytics.getInstance(DashboardActivity.this);
                                Bundle paramsrateoutsid = new Bundle();
                                snalyticsRateoutside.logEvent("Rate outsid Clicked", paramsrateoutsid);
                        }
                    }
                })
                .monitor();

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this);
    }
}
