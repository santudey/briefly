package com.latest.news.hunt.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Adapter.NewsDetailsAdapter;
import com.latest.news.hunt.Fragment.FeedFragment;
import com.latest.news.hunt.Fragment.SavedFragment;
import com.latest.news.hunt.Fragment.TrendingFragment;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.CommonUtility;
import com.latest.news.hunt.Util.ConnectionDetectorForActivity;
import com.latest.news.hunt.Util.HidingScrollListener;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.Result;
import com.latest.news.hunt.WebService.Rest_Client;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 25/5/16.
 */
public class NewsDetailActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayoutManager layoutManager;
    private RecyclerView mRecyclerview;
    private SimpleDraweeView dvNewsImage;
    private WebView wvPublisherSite;
    private RelativeLayout rlShare;
    private RelativeLayout rlBack;
    private RelativeLayout noDataOverlay;
    private ImageView ivInternet;
    private TextView noDataTitle;
    private TextView noDataSubTitle;
    private TextView noDataTv;
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;
    private String url = "";
    private String news_feature_image = "";
    NewsDetailsAdapter adapter;
    List< Result > newsList;
    ConnectionDetectorForActivity connectionDetector;
    LinearLayout lnrToolBar;
    LinearLayout lnrFooter;
    RelativeLayout rlFacebook;
    RelativeLayout rlTwitter;
    RelativeLayout rlgmail;
    RelativeLayout rlWhatsApp;
    RelativeLayout rlMore;
//    ImageView ivFullscreen;
    String headline = "";
    String noti_push = "";
    PrefsManager prefsManager;
    ProgressWheel progress_bar;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setTheme( R.style.AppTheme );
        if( Build.VERSION.SDK_INT >= 21 ) {
            getWindow( ).setNavigationBarColor( getResources( ).getColor( R.color.blue_news10 ) );
            getWindow( ).setStatusBarColor( getResources( ).getColor( R.color.blue_news10 ) );
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fresco.initialize( this );
        setContentView( R.layout.activity_news_details );
        if( getIntent( ).hasExtra( "url" ) ) {
            url = getIntent( ).getStringExtra( "url" );
        }
        if( getIntent( ).hasExtra( "news_feature_image" ) ) {
            news_feature_image = getIntent( ).getStringExtra( "news_feature_image" );
        }
        if(getIntent().hasExtra( "noti_push" )) {
            noti_push = getIntent().getStringExtra("noti_push");
        }
        CommonUtility.feedIsResumed = true;
        CommonUtility.savedIsResumed = true;
        CommonUtility.trendingIsResumed = true;
        init( );
        progress_bar.setVisibility( View.VISIBLE );
        getNewsDetails( );
        mRecyclerview.addOnScrollListener( new HidingScrollListener( ) {
            @Override
            public void onHide( ) {
                hideViews( );
            }

            @Override
            public void onShow( ) {
                showViews( );
            }
        } );

        ivInternet.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                progress_bar.setVisibility( View.VISIBLE );
                getNewsDetails( );
            }
        } );
    }

    private void init( ) {
        AppEventsLogger.activateApp(this);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        mFirebaseAnalytics.logEvent("News Details Page Visit", bundle);

        prefsManager = new PrefsManager( this );
        fontRegular = Typeface.createFromAsset(getAssets( ),
                "fonts/Roboto-Regular.ttf");
        fontBold = Typeface.createFromAsset(getAssets( ),
                "fonts/Roboto-Bold.ttf");
        fontMedium = Typeface.createFromAsset(getAssets( ),
                "fonts/Roboto-Medium.ttf");
        noDataOverlay = ( RelativeLayout ) findViewById( R.id.noDataOverlay );
        noDataTv = ( TextView ) findViewById( R.id.noDataTv );
        noDataTitle = ( TextView ) findViewById( R.id.noDataTitle );
        noDataSubTitle = ( TextView ) findViewById( R.id.noDataSubTitle );
        ivInternet = ( ImageView ) findViewById( R.id.ivInternet );
        noDataTv.setTypeface( fontRegular );
        noDataTitle.setTypeface( fontRegular );
        noDataSubTitle.setTypeface( fontRegular );
        noDataOverlay.setVisibility( View.GONE );
        progress_bar = ( ProgressWheel ) findViewById( R.id.progress_bar );
        lnrToolBar = ( LinearLayout ) findViewById( R.id.lnrToolBar );
        lnrFooter = ( LinearLayout ) findViewById( R.id.lnrFooter );
        rlFacebook = ( RelativeLayout ) findViewById( R.id.rlFacebook );
        rlTwitter = ( RelativeLayout ) findViewById( R.id.rlTwitter );
        rlgmail = ( RelativeLayout ) findViewById( R.id.rlgmail );
        rlWhatsApp = ( RelativeLayout ) findViewById( R.id.rlWhatsApp );
        rlMore = ( RelativeLayout ) findViewById( R.id.rlMore );
        rlShare = ( RelativeLayout ) findViewById( R.id.rlShare );
        rlBack = ( RelativeLayout ) findViewById( R.id.rlBack );
//        ivFullscreen = ( ImageView ) findViewById( R.id.ivFullscreen );
        connectionDetector = new ConnectionDetectorForActivity( this );
        newsList = new ArrayList<>( );
        dvNewsImage = ( SimpleDraweeView ) findViewById( R.id.dvNewsImage );
        mRecyclerview = ( RecyclerView ) findViewById( R.id.mRecyclerview );
        wvPublisherSite = ( WebView ) findViewById( R.id.wvPublisherSite );
        layoutManager = new LinearLayoutManager( this );
        mRecyclerview.setLayoutManager( layoutManager );
        rlFacebook.setOnClickListener( this );
        rlTwitter.setOnClickListener( this );
        rlgmail.setOnClickListener( this );
        rlWhatsApp.setOnClickListener( this );
        rlMore.setOnClickListener( this );
        rlBack.setOnClickListener( this );
        rlShare.setOnClickListener( this );
//        ivFullscreen.setOnClickListener( this );
    }

    private void getNewsDetails( ) {
        try {
            Rest_Client.get( ).getNewsDetails( prefsManager.getAccessToken(), url, new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    noDataOverlay.setVisibility( View.GONE );
                    Uri CoverimageUri = Uri.parse( news_feature_image );
                    dvNewsImage.setImageURI( CoverimageUri );
                    headline = mainPojo.getResults( ).get( 0 ).getTitle();
                    if( String.valueOf( mainPojo.getResults( ).get( 0 ).getNews_html_text( ) ) != null || !String.valueOf( mainPojo.getResults( ).get( 0 ).getNews_html_text( ) ).equals( "" ) ) {
                        adapter = new NewsDetailsAdapter( newsList, NewsDetailActivity.this,
                                String.valueOf( mainPojo.getResults( ).get( 0 ).getNewsPubdate( ) ),
                                String.valueOf( mainPojo.getResults( ).get( 0 ).getNews_publisher_logo( ) ),
                                String.valueOf( mainPojo.getResults( ).get( 0 ).getTitle( ) ),
                                String.valueOf( mainPojo.getResults( ).get( 0 ).getNews_html_text( ) ),
                                String.valueOf( mainPojo.getResults( ).get( 0 ).getNews_publisher( ) ),
                                NewsDetailActivity.this );
                        mRecyclerview.setAdapter( adapter );
                        getRelatedNews( );
                    } else {
                        wvPublisherSite.getSettings( ).setJavaScriptEnabled( true );
                        wvPublisherSite.setWebViewClient( new WebViewClient( ) );
                        wvPublisherSite.setWebChromeClient( new WebChromeClient( ) {
                            public void onProgressChanged( WebView view, int progress ) {

                            }
                        } );
                        wvPublisherSite.setWebViewClient( new WebViewClient( ) {
                            @Override
                            public void onPageFinished( WebView view, String url ) {
                                super.onPageFinished( view, url );
                            }
                        } );
                        wvPublisherSite.loadUrl( url );
                        WebSettings settings = wvPublisherSite.getSettings( );
                        settings.setJavaScriptEnabled( true );
                        settings.setDomStorageEnabled( true );
                        settings.setUserAgentString( "Mozilla/5.0 (Linux; U;Android 2.0; en-us; Droid Build/ESD20) " + "AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17" );
                    }
                }

                @Override
                public void failure( RetrofitError error ) {
                    noDataOverlay.setVisibility( View.VISIBLE );
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }

    private void getRelatedNews( ) {
        try {
            Rest_Client.get( ).getRelatedNews( prefsManager.getAccessToken(), url, new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    noDataOverlay.setVisibility( View.GONE );
                    progress_bar.setVisibility( View.GONE );
                    newsList.addAll( mainPojo.getResults( ) );
                    adapter.notifyDataSetChanged( );
                }

                @Override
                public void failure( RetrofitError error ) {
                    noDataOverlay.setVisibility( View.VISIBLE );
                    progress_bar.setVisibility( View.GONE );
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }

    public void hideViews( ) {
        lnrToolBar.animate( ).translationY( -lnrToolBar.getHeight( ) ).setInterpolator( new AccelerateInterpolator( 2 ) );

        FrameLayout.LayoutParams lp = ( FrameLayout.LayoutParams ) lnrFooter.getLayoutParams( );
        int fabBottomMargin = lp.bottomMargin;
        lnrFooter.animate( ).translationY( lnrFooter.getHeight( ) + fabBottomMargin ).setInterpolator( new AccelerateInterpolator( 2 ) ).start( );
    }

    public void showViews( ) {
        lnrToolBar.animate( ).translationY( 0 ).setInterpolator( new DecelerateInterpolator( 2 ) );
        lnrFooter.animate( ).translationY( 0 ).setInterpolator( new DecelerateInterpolator( 2 ) ).start( );
    }

    @Override
    public void onClick( View v ) {
        switch( v.getId() ) {
            case R.id.rlFacebook :
                AppEventsLogger.activateApp(this);
                FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
                Bundle bundle = new Bundle();
                mFirebaseAnalytics.logEvent("Facebook Share Click", bundle);
                CommonUtility.shareByFacebookMessanger( this, headline + " for more download : https://play.google.com/store/apps/details?id=com.latest.news.hunt" );
                break;
            case R.id.rlTwitter :
                AppEventsLogger.activateApp(this);
                FirebaseAnalytics mFirebaseAnalyticsTwitter = FirebaseAnalytics.getInstance(this);
                Bundle bundleTwitter = new Bundle();
                mFirebaseAnalyticsTwitter.logEvent("Twitter Share Click", bundleTwitter);
                CommonUtility.shareByTwitter( this, headline + " for more download : https://play.google.com/store/apps/details?id=com.latest.news.hunt");
                                break;
            case R.id.rlgmail :
                AppEventsLogger.activateApp(this);
                FirebaseAnalytics mFirebaseAnalyticsGmail = FirebaseAnalytics.getInstance(this);
                Bundle bundleGmail = new Bundle();
                mFirebaseAnalyticsGmail.logEvent("Gmail Share Click", bundleGmail);
                CommonUtility.shareBygmail( this, "Important News", CommonUtility.getEmailId( this ),
                        headline + " for more download : https://play.google.com/store/apps/details?id=com.latest.news.hunt","");
                break;
            case R.id.rlWhatsApp :
                AppEventsLogger.activateApp(this);
                FirebaseAnalytics mFirebaseAnalyticsWhatsApp = FirebaseAnalytics.getInstance(this);
                Bundle bundleWhatsApp = new Bundle();
                mFirebaseAnalyticsWhatsApp.logEvent("WhatsApp Share Click", bundleWhatsApp);
                CommonUtility.shareByWhatsApp( this, headline + " for more download : https://play.google.com/store/apps/details?id=com.latest.news.hunt" );
                break;
            case R.id.rlMore :
                AppEventsLogger.activateApp(this);
                FirebaseAnalytics mFirebaseAnalyticsMore = FirebaseAnalytics.getInstance(this);
                Bundle bundleMore = new Bundle();
                mFirebaseAnalyticsMore.logEvent("More Share Click", bundleMore);
                CommonUtility.shareinSocialApp( this, headline + " for more download : https://play.google.com/store/apps/details?id=com.latest.news.hunt" );
                break;
            case R.id.rlBack :
                AppEventsLogger.activateApp(this);
                FirebaseAnalytics mFirebaseAnalyticsFinish = FirebaseAnalytics.getInstance(this);
                Bundle bundleFinish = new Bundle();
                mFirebaseAnalyticsFinish.logEvent("Finish Share Click", bundleFinish);
                if(!noti_push.equals( "" )) {
                    CommonUtility.feedIsResumed = false;
                    CommonUtility.savedIsResumed = false;
                    CommonUtility.trendingIsResumed = false;
                    Intent intent = new Intent( this, DashboardActivity.class );
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity( intent );
                    finish();
                } else {
                    finish( );
                }
                break;
            case R.id.rlShare:
                AppEventsLogger.activateApp(this);
                FirebaseAnalytics mFirebaseAnalyticsTopShatre = FirebaseAnalytics.getInstance(this);
                Bundle bundleTopShatre = new Bundle();
                mFirebaseAnalyticsTopShatre.logEvent("TopShatre Share Click", bundleTopShatre);
                CommonUtility.shareinSocialApp( this, headline + " for more download : https://play.google.com/store/apps/details?id=com.latest.news.hunt" );
                break;
            default:
                break;
        }
    }
}
