package com.latest.news.hunt.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Adapter.FeedAdapter;
import com.latest.news.hunt.Adapter.SavedAdapter;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.CommonUtility;
import com.latest.news.hunt.Util.ConnectionDetector;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.Result;
import com.latest.news.hunt.WebService.Rest_Client;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 3/5/16.
 */
public class SavedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout mSwiperefreshLayout;
    private RecyclerView mRecyclerview;
    private TextView noDataTitle;
    private TextView noDataSubTitle;
    private RelativeLayout noDataOverlay;
    private LinearLayoutManager mLayoutManager;
    ConnectionDetector connectionDetector;
    List< Result > feedList;
    SavedAdapter adapter;
    Context context;
    private boolean mIsApiCall = false;
    int mPageNo = 0;
    public static boolean isnomore = false;
    PrefsManager prefsManager;
    ProgressWheel progress_bar;
    Typeface fontRegular;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        Fresco.initialize( getActivity( ) );
        final View view = inflater.inflate( R.layout.fragment_saved, container, false );
        context = getActivity( );
        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            CommonUtility.feedIsResumed = false;
          if( CommonUtility.isLiked) {
              CommonUtility.isLiked = false;
              mPageNo = 0;
              isnomore = false;
              scrollApiCall( mPageNo );
          }
        }
    }

    @Override
    public void onActivityCreated( @Nullable Bundle savedInstanceState ) {
        super.onActivityCreated( savedInstanceState );
        if( context != null ) {
            AppEventsLogger.activateApp(getActivity());
            FirebaseAnalytics saved = FirebaseAnalytics.getInstance(getActivity());
            Bundle bundlesaved = new Bundle();
            saved.logEvent("Feed Fragment", bundlesaved);
            initialize( getView( ) );
            progress_bar.setVisibility( View.VISIBLE );
            adapter = new SavedAdapter( feedList, context );
            mRecyclerview.setAdapter( adapter );
            mSwiperefreshLayout.setOnRefreshListener( this );
            mRecyclerview.setOnScrollListener( new RecyclerView.OnScrollListener( ) {
                @Override
                public void onScrollStateChanged( RecyclerView recyclerView, int newState ) {
                    super.onScrollStateChanged( recyclerView, newState );
                }

                @Override
                public void onScrolled( RecyclerView recyclerView, int dx, int dy ) {
                    super.onScrolled( recyclerView, dx, dy );
                    int totalItems = mLayoutManager.getItemCount( );
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition( );
                    if( isnomore == false && !mIsApiCall && totalItems - 1 == lastVisibleItem ) {
                        mIsApiCall = true;
                        Log.e( "TotalItems: ", totalItems + "" );
                        scrollApiCall( mPageNo += 3 );
                    }
                }
            } );
        }
    }

    private void initialize( View view ) {
        fontRegular = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Regular.ttf");
        prefsManager = new PrefsManager( context );
        feedList = new ArrayList<>( );
        connectionDetector = new ConnectionDetector( getActivity( ) );
        progress_bar = ( ProgressWheel ) view.findViewById( R.id.progress_bar );
        noDataOverlay = ( RelativeLayout ) view.findViewById( R.id.noDataOverlay );
        noDataTitle = ( TextView ) view.findViewById( R.id.noDataTitle );
        noDataSubTitle = ( TextView ) view.findViewById( R.id.noDataSubTitle );
        mRecyclerview = ( RecyclerView ) view.findViewById( R.id.mRecyclerview );
        mSwiperefreshLayout = ( SwipeRefreshLayout ) view.findViewById( R.id.mSwiperefreshLayout );
        mSwiperefreshLayout.setColorSchemeColors( getResources( ).getColor( R.color.cpb_blue ) );
        mLayoutManager = new LinearLayoutManager( getActivity( ) );
        mRecyclerview.setItemAnimator( new DefaultItemAnimator( ) );
        mRecyclerview.setLayoutManager( mLayoutManager );
        noDataTitle.setTypeface( fontRegular );
        noDataSubTitle.setTypeface( fontRegular );
    }

    @Override
    public void onRefresh( ) {
        isnomore = false;
        mPageNo = 0;
        scrollApiCall( mPageNo );
    }

    @Override
    public void onResume( ) {
        super.onResume( );
        if( !CommonUtility.savedIsResumed) {
            mPageNo = 0;
            isnomore = false;
            scrollApiCall( mPageNo );
        }
    }

    private void scrollApiCall( final int mPageNo ) {
        try {
            Rest_Client.get( ).getSavedFeed( 3, mPageNo,prefsManager.getAccessToken(), new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    progress_bar.setVisibility( View.GONE );
                    if( mainPojo.getResults( ).size( ) < 3 && mainPojo.getResults( ).size( ) > 0 ) {
                        isnomore = true;
                    }
                    if( mSwiperefreshLayout.isRefreshing( ) ) {
                        mSwiperefreshLayout.setRefreshing( false );
                    }
                    if( mPageNo == 0 ) {
                        feedList.clear( );
                    }
                    feedList.addAll( mainPojo.getResults( ) );
                    if(feedList.size() == 0) {
                        noDataOverlay.setVisibility( View.VISIBLE );
                    } else {
                        noDataOverlay.setVisibility( View.GONE );
                    }
                    adapter.notifyDataSetChanged( );
                    mIsApiCall = false;
                }

                @Override
                public void failure( RetrofitError error ) {
                    progress_bar.setVisibility( View.GONE );
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
          e.printStackTrace();
        }
    }
}