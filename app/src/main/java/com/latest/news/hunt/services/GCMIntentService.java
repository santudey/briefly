package com.latest.news.hunt.services;


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.latest.news.hunt.Activity.DashboardActivity;
import com.latest.news.hunt.Activity.NewsDetailActivity;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.receiver.GCMBroadcastReceiver;


public class GCMIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    private PrefsManager mPrefs;
    //    private ApplicationGlobal mGlobal;
    private String examKey = "examKey";
    private String flagAcademicvideo = "flagAcademicvideo";
    private String flagVideo = "flagVideo";
    private String flagAudio = "flagAudio";
    private String flagFaculty = "flagFaculty";
    private String id = "id";

    public GCMIntentService( ) {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance( this );
        int requestId = (int) System.currentTimeMillis();
        String messageType = gcm.getMessageType(intent);

        mPrefs = new PrefsManager(this);
//        mGlobal= (ApplicationGlobal) getApplication();

        if (!extras.isEmpty()) {

            if ( GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                mNotificationManager = (NotificationManager)
                        this.getSystemService(Context.NOTIFICATION_SERVICE);

                try {
                    String news_title = "";
                    String newslink = "";
                    String news_feature_image = "";
                    Intent pushIntent = null;
                    try {
                         news_title = extras.getString("news_title");
                    } catch( Exception e ) {}

                    try {
                         newslink = extras.getString( "newslink" );
                    } catch( Exception e ) {}

                    try {
                        news_feature_image = extras.getString( "fetaures_img" );
                    } catch( Exception e ) {}

                    if( !newslink.equals( "" ) ) {
                        pushIntent = new Intent(this, NewsDetailActivity.class);
                        pushIntent.putExtra("url", newslink);
                        pushIntent.putExtra("news_feature_image", news_feature_image);
                        pushIntent.putExtra("noti_push", "noti_push");
                    } else {
                        pushIntent = new Intent(this, DashboardActivity.class);
                        pushIntent.putExtra("noti_push", "noti_push");
                    }
                    pushIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    pushIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent contentIntent = PendingIntent.getActivity(this, requestId, pushIntent, 0);
                    setNotification( contentIntent, news_title );
                } catch (Exception e) {
                    e.printStackTrace();
                }

                GCMBroadcastReceiver.completeWakefulIntent( intent );
            }
        }
    }

    private void setNotification(PendingIntent pushIntent, String message) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon( getNotificationIcon())
                .setContentTitle( getString( R.string.app_name ) )
                .setStyle( new NotificationCompat.BigTextStyle( ).bigText( message ) )
                .setContentText( message )
                .setDefaults( Notification.DEFAULT_SOUND );

        mBuilder.setContentIntent(pushIntent);
        mBuilder.setAutoCancel(true);
        mNotificationManager.cancelAll();
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.transparent_logo : R.drawable.logo;
    }
}

