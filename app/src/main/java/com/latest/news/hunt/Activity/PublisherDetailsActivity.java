package com.latest.news.hunt.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Adapter.PublisherDetailsAdapter;
import com.latest.news.hunt.Fragment.FeedFragment;
import com.latest.news.hunt.Fragment.SavedFragment;
import com.latest.news.hunt.Fragment.SubscriptionFragment;
import com.latest.news.hunt.Fragment.TrendingFragment;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.CommonUtility;
import com.latest.news.hunt.Util.ConnectionDetectorForActivity;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.Result;
import com.latest.news.hunt.WebService.Rest_Client;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 28/5/16.
 */
public class PublisherDetailsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Toolbar toolbar;
    private TextView tvToolbarTitle;
    private TextView tvFollowing;
    private RelativeLayout noDataOverlay;
    private ImageView ivInternet;
    private TextView noDataTitle;
    private TextView noDataSubTitle;
    private TextView noDataTv;
    SwipeRefreshLayout mSwiperefreshLayout;
    RecyclerView mRecyclerview;
    LinearLayoutManager layoutManager;
    List< Result > feedList;
    ConnectionDetectorForActivity connectionDetector;
    private boolean mIsApiCall = false;
    int mPageNo = 0;
    public static boolean isnomore = false;
    public static boolean emptylist = false;
    PublisherDetailsAdapter adapter;
    ProgressWheel progress_bar;
    String publisherName = "";
    String Publisher_logo = "";
    String subscribe_status = "";
    String no_of_people_subscribe = "";
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;
    boolean flagSubscribe = false;
    PrefsManager prefsManager;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setTheme( R.style.AppTheme );
        if( Build.VERSION.SDK_INT >= 21 ) {
            getWindow( ).setNavigationBarColor( getResources( ).getColor( R.color.blue_news10 ) );
            getWindow( ).setStatusBarColor( getResources( ).getColor( R.color.blue_news10 ) );
        }
        setContentView( R.layout.activity_publisher_details );
        if( getIntent( ).hasExtra( "publisherName" ) ) {
            publisherName = getIntent( ).getStringExtra( "publisherName" );
        }
        CommonUtility.feedIsResumed = true;
        CommonUtility.savedIsResumed = true;
        CommonUtility.trendingIsResumed = true;
        init( );
        isnomore = false;
        if( feedList.size( ) == 0 ) {
            emptylist = true;
        }
        progress_bar.setVisibility( View.VISIBLE );
        getPublisherProfile( );
        mSwiperefreshLayout.setOnRefreshListener( this );
        mRecyclerview.setOnScrollListener( new RecyclerView.OnScrollListener( ) {
            @Override
            public void onScrollStateChanged( RecyclerView recyclerView, int newState ) {
                super.onScrollStateChanged( recyclerView, newState );
            }

            @Override
            public void onScrolled( RecyclerView recyclerView, int dx, int dy ) {
                super.onScrolled( recyclerView, dx, dy );
                int totalItems = layoutManager.getItemCount( );
                int lastVisibleItem = layoutManager.findLastVisibleItemPosition( );
                if( isnomore == false && !mIsApiCall && totalItems - 1 == lastVisibleItem ) {
                    mIsApiCall = true;
                    Log.e( "TotalItems: ", totalItems + "" );
                    scrollApiCall( mPageNo += 3, publisherName );
                }
            }
        } );

        ivInternet.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                AppEventsLogger.activateApp(PublisherDetailsActivity.this);
                FirebaseAnalytics mFirebaseAnalyticsPublisherDetails = FirebaseAnalytics.getInstance(PublisherDetailsActivity.this);
                Bundle bundlePublisherDetails = new Bundle();
                mFirebaseAnalyticsPublisherDetails.logEvent("Publisher Dtls No Internet Click", bundlePublisherDetails);
                progress_bar.setVisibility( View.VISIBLE );
                isnomore = false;
                if( feedList.size( ) == 0 ) {
                    emptylist = true;
                }
                progress_bar.setVisibility( View.VISIBLE );
                getPublisherProfile( );
            }
        } );

        tvFollowing.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                if(SubscriptionFragment.mySubscriptionSize > 1) {
                    AppEventsLogger.activateApp(PublisherDetailsActivity.this);
                    FirebaseAnalytics mFirebaseAnalyticsPublisherFollowing = FirebaseAnalytics.getInstance(PublisherDetailsActivity.this);
                    Bundle bundlePublisherDFollow = new Bundle();
                    mFirebaseAnalyticsPublisherFollowing.logEvent("Publisher Dtls Subscribe Click", bundlePublisherDFollow);
                    if( flagSubscribe == false ) {
                        flagSubscribe = true;
                        if(subscribe_status.equals( "true" )) {
                            subscribe_status = "false" ;
                            tvFollowing.setText( getResources().getString( R.string.subscribe ) );
                        } else {
                            subscribe_status = "true";
                            tvFollowing.setText( getResources().getString( R.string.subscribed ) );
                        }

                        doSubscribe( );
                    }
                } else {
                    Toast.makeText( PublisherDetailsActivity.this,"Need atleast One subscription",Toast.LENGTH_SHORT ).show();
                }
            }
        } );
    }

    private void init( ) {
        AppEventsLogger.activateApp(this);
        FirebaseAnalytics mFirebaseAnalyticsPublisherDetails = FirebaseAnalytics.getInstance(this);
        Bundle bundlePublisherDetails = new Bundle();
        mFirebaseAnalyticsPublisherDetails.logEvent("Publisher Details Page", bundlePublisherDetails);

        SubscriptionFragment.isLoad = true;
        prefsManager = new PrefsManager( this );
        toolbar = ( Toolbar ) findViewById( R.id.toolbar );
        toolbar.setTitle( "" );
        setSupportActionBar( toolbar );
        ActionBar bar = getSupportActionBar( );
        if( bar != null ) {
            bar.setHomeButtonEnabled( true );
            bar.setDisplayHomeAsUpEnabled( true );
        }
        progress_bar = ( ProgressWheel ) findViewById( R.id.progress_bar );
        tvToolbarTitle = ( TextView ) findViewById( R.id.tvToolbarTitle );
        tvFollowing = ( TextView ) findViewById( R.id.tvFollowing );
        toolbar.setNavigationOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                finish( );
            }
        } );
        fontRegular = Typeface.createFromAsset(getAssets( ),
                "fonts/Roboto-Regular.ttf");
        fontBold = Typeface.createFromAsset(getAssets( ),
                "fonts/Roboto-Bold.ttf");
        fontMedium = Typeface.createFromAsset(getAssets( ),
                "fonts/Roboto-Medium.ttf");
        tvToolbarTitle.setTypeface( fontMedium );
        tvFollowing.setTypeface( fontRegular );
        noDataOverlay = ( RelativeLayout ) findViewById( R.id.noDataOverlay );
        noDataTv = ( TextView ) findViewById( R.id.noDataTv );
        noDataTitle = ( TextView ) findViewById( R.id.noDataTitle );
        noDataSubTitle = ( TextView ) findViewById( R.id.noDataSubTitle );
        ivInternet = ( ImageView ) findViewById( R.id.ivInternet );
        noDataTv.setTypeface( fontRegular );
        noDataTitle.setTypeface( fontRegular );
        noDataSubTitle.setTypeface( fontRegular );
        noDataOverlay.setVisibility( View.GONE );
        feedList = new ArrayList<>( );
        connectionDetector = new ConnectionDetectorForActivity( this );
        mSwiperefreshLayout = ( SwipeRefreshLayout ) findViewById( R.id.mSwiperefreshLayout );
        mRecyclerview = ( RecyclerView ) findViewById( R.id.mRecyclerview );
        layoutManager = new LinearLayoutManager( this );
        mRecyclerview.setItemAnimator( new DefaultItemAnimator( ) );
        mSwiperefreshLayout.setColorSchemeColors( getResources( ).getColor( R.color.cpb_blue ) );
        mRecyclerview.setLayoutManager( layoutManager );
        tvToolbarTitle.setText( publisherName );
    }

    @Override
    public void onRefresh( ) {
        isnomore = false;
        mPageNo = 0;
        scrollApiCall( mPageNo, publisherName );
    }

    private void scrollApiCall( final int mPageNo, String publisherName ) {
        try {
            Rest_Client.get( ).getPublisherFeed( prefsManager.getAccessToken(), mPageNo, 3, publisherName, new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    noDataOverlay.setVisibility( View.GONE );
                    progress_bar.setVisibility( View.GONE );
                    if( mainPojo.getResults( ).size( ) < 3 ) {
                        isnomore = true;
                    }
                    if( mSwiperefreshLayout.isRefreshing( ) ) {
                        mSwiperefreshLayout.setRefreshing( false );
                    }
                    if( mPageNo == 0 ) {
                        feedList.clear( );
                    }
                    feedList.addAll( mainPojo.getResults( ) );
                    adapter.notifyDataSetChanged( );
                    mIsApiCall = false;
                }

                @Override
                public void failure( RetrofitError error ) {
                    noDataOverlay.setVisibility( View.VISIBLE );
                    progress_bar.setVisibility( View.GONE );
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }

    private void getPublisherProfile( ) {
        try {
            Rest_Client.get( ).getPublisherProfile( prefsManager.getAccessToken(), publisherName, new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    noDataOverlay.setVisibility( View.GONE );
                    Publisher_logo = mainPojo.getResults( ).get( 0 ).getPublisher_logo( );
                    subscribe_status = mainPojo.getResults( ).get( 0 ).getSubscribe_status( );
                    no_of_people_subscribe = mainPojo.getResults( ).get( 0 ).getNo_of_people_subscribe( );
                    if(subscribe_status.equals( "true" )) {
                        tvFollowing.setText( getResources().getString( R.string.subscribed ) );
                    } else {
                        tvFollowing.setText( getResources().getString( R.string.subscribe ) );
                    }
                    adapter = new PublisherDetailsAdapter( feedList, Publisher_logo, subscribe_status, no_of_people_subscribe, publisherName, PublisherDetailsActivity.this );
                    mRecyclerview.setAdapter( adapter );
                    mPageNo = 0;
                    scrollApiCall( mPageNo, publisherName );
                }

                @Override
                public void failure( RetrofitError error ) {
                    noDataOverlay.setVisibility( View.VISIBLE );
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }

    private void doSubscribe( ) {
        try {
            Rest_Client.get( ).doSubscribe( prefsManager.getAccessToken(), publisherName, new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    noDataOverlay.setVisibility( View.GONE );
                    if( mainPojo.getStatus( ).equals( "Success" ) ) {
                        flagSubscribe = false;
                        if( mainPojo.getResponse( ).equals( "Sucessfully subscribed" ) && subscribe_status.equals( "false" ) ) {
                            subscribe_status = "true" ;
                            tvFollowing.setText( getResources().getString( R.string.subscribed ) );
                        } else if( mainPojo.getResponse( ).equals( "Sucessfully Unsubscribe" ) && subscribe_status.equals( "true" ) ) {
                            subscribe_status = "false" ;
                            tvFollowing.setText( getResources().getString( R.string.subscribe ) );
                        }
                    } else {
                        flagSubscribe = false;
                        if( subscribe_status.equals( "false"  )  ) {
                            subscribe_status = "true";
                            tvFollowing.setText( getResources().getString( R.string.subscribed ) );
                        } else {
                            subscribe_status = "false";
                            tvFollowing.setText( getResources().getString( R.string.subscribe ) );
                        }
                    }
                }

                @Override
                public void failure( RetrofitError error ) {
                    noDataOverlay.setVisibility( View.VISIBLE );
                    flagSubscribe = false;
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }
}
