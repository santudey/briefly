package com.latest.news.hunt.WebService.Pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sushovan on 29/3/16.
 */
public class MainPojo {

    public String authkey = "";

    public Float last_updated;
    public String country;

    public String first_news_time;
    public String request;
    public List<Result> results = new ArrayList<Result>();
    public String offsetstart;
    public String no_requestednews;
    public Integer api_version;
    public String result;
    public String status;
    public String response;

    public String getAuthkey( ) {
        return authkey;
    }

    public void setAuthkey( String authkey ) {
        this.authkey = authkey;
    }

    public String getStatus( ) {
        return status;
    }

    public void setStatus( String status ) {
        this.status = status;
    }

    public String getResponse( ) {
        return response;
    }

    public void setResponse( String response ) {
        this.response = response;
    }

    public String getFirst_news_time( ) {
        return first_news_time;
    }

    public void setFirst_news_time( String first_news_time ) {
        this.first_news_time = first_news_time;
    }

    public String getOffsetstart( ) {
        return offsetstart;
    }

    public void setOffsetstart( String offsetstart ) {
        this.offsetstart = offsetstart;
    }

    public String getNo_requestednews( ) {
        return no_requestednews;
    }

    public void setNo_requestednews( String no_requestednews ) {
        this.no_requestednews = no_requestednews;
    }

    public Integer getApi_version( ) {
        return api_version;
    }

    public void setApi_version( Integer api_version ) {
        this.api_version = api_version;
    }

    public String getResult( ) {
        return result;
    }

    public void setResult( String result ) {
        this.result = result;
    }

    public Float getLast_updated( ) {
        return last_updated;
    }

    public void setLast_updated( Float last_updated ) {
        this.last_updated = last_updated;
    }

    public String getRequest( ) {
        return request;
    }

    public void setRequest( String request ) {
        this.request = request;
    }

    public List< Result > getResults( ) {
        return results;
    }

    public void setResults( List< Result > results ) {
        this.results = results;
    }

    public String getCountry( ) {
        return country;
    }

    public void setCountry( String country ) {
        this.country = country;
    }

}
