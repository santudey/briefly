package com.latest.news.hunt.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.R;

/**
 * Created by sushovan on 9/4/16.
 */
public class AppupdateDialogFragment extends DialogFragment {
    private TextView tvTitle;
    private TextView tvLater;
    private TextView tvUpdate;
    Typeface fontregular;
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature( Window.FEATURE_NO_TITLE );
        getDialog( ).setCanceledOnTouchOutside( false );
        View view = inflater.inflate ( R.layout.popup_appupdate,container,
                false);
        tvTitle = ( TextView ) view.findViewById( R.id.tvTitle );
        tvLater = ( TextView ) view.findViewById( R.id.tvLater );
        tvUpdate = ( TextView ) view.findViewById( R.id.tvUpdate );
        fontregular = Typeface.createFromAsset(getActivity().getAssets( ),
                "fonts/Roboto-Regular.ttf");
        tvTitle.setTypeface( fontregular );
        tvLater.setTypeface( fontregular );
        tvUpdate.setTypeface( fontregular );
        return view;
    }

    @Override
    public void onActivityCreated( Bundle savedInstanceState ) {
        super.onActivityCreated( savedInstanceState );
        tvLater.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                dismiss();
            }
        } );
        tvUpdate.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                AppEventsLogger.activateApp(getActivity());
                FirebaseAnalytics appupdate = FirebaseAnalytics.getInstance(getActivity());
                Bundle bundleappupdate = new Bundle();
                appupdate.logEvent("Appupdate Click", bundleappupdate);

                Uri uri = Uri.parse( "market://details?id=" + getActivity().getPackageName( ) );
                Intent goToMarket = new Intent( Intent.ACTION_VIEW, uri );
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags( Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK );
                try {
                    startActivity( goToMarket );
                } catch( ActivityNotFoundException e ) {
                    startActivity( new Intent( Intent.ACTION_VIEW,
                            Uri.parse( "http://play.google.com/store/apps/details?id=" + getActivity().getPackageName( ) ) ) );
                }
            }
        } );

    }
}
