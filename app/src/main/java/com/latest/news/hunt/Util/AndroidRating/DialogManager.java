package com.latest.news.hunt.Util.AndroidRating;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

final class DialogManager {

    private DialogManager() {
    }

    static Dialog create(final Context context, DialogOptions options) {
        AlertDialog.Builder builder = Utils.getDialogBuilder(context);

        if (options.shouldShowTitle()) builder.setTitle(options.getTitleResId());

        builder.setCancelable(options.getCancelable());

        View view = options.getView();
        if (view != null) {
            builder.setView(view);
        }
        else {
            builder.setMessage(options.getMessageResId());
        }
        final OnClickButtonListener listener = options.getListener();

        builder.setPositiveButton(options.getTextPositiveResId(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(IntentHelper.createIntentForGooglePlay(context));
                PreferenceHelper.setAgreeShowDialog(context, false);
                if( listener != null ) listener.onClickButton(which);
            }
        });

        if (options.shouldShowNeutralButton()) {
            builder.setNeutralButton(options.getTextNeutralResId(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PreferenceHelper.setRemindInterval(context);
                    if (listener != null) listener.onClickButton(which);
                }
            });
        }

        /*builder.setNegativeButton(options.getTextNegativeResId(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PreferenceHelper.setAgreeShowDialog(context, false);
                if (listener != null) listener.onClickButton(which);
            }
        });*/

        Dialog alert = builder.create();
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                PreferenceHelper.setRemindInterval(context);
                if( listener != null ) listener.onClickButton(-4);
            }
        });
        return alert;
    }

}