package com.latest.news.hunt.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.latest.news.hunt.Fragment.FeedFragment;
import com.latest.news.hunt.Fragment.SavedFragment;
import com.latest.news.hunt.Fragment.SubscriptionFragment;
import com.latest.news.hunt.Fragment.TrendingFragment;

/**
 * Created by sushovan on 3/5/16.
 */
public class DashboardPagerAdapter extends FragmentStatePagerAdapter {
    int noOfTab;

    public DashboardPagerAdapter( FragmentManager fm, int noOfTab ) {
        super( fm );
        this.noOfTab = noOfTab;
    }

    @Override
    public Fragment getItem( int position ) {
        switch( position ) {
            case 0:
                FeedFragment feedFragment = new FeedFragment();
                return feedFragment;
            case 1:
                TrendingFragment trendingFragment = new TrendingFragment();
                return trendingFragment;
            case 2:
                SavedFragment savedFragment = new SavedFragment();
                return savedFragment;
            case 3:
                SubscriptionFragment subscriptionFragment = new SubscriptionFragment();
                return subscriptionFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount( ) {
        return noOfTab;
    }
}
