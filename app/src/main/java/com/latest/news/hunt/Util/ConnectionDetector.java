package com.latest.news.hunt.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.latest.news.hunt.Activity.DashboardActivity;

import retrofit.RetrofitError;


/**
 * Created by HOME on 3/3/2015.
 */
public class ConnectionDetector {

    private Context _context;
    private PrefsManager mprefs;

    public ConnectionDetector(Context context) {
        this._context = context;
        mprefs=new PrefsManager(context);
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null
                    && activeNetwork.isConnected();

            return isConnected;
        }
        return false;
    }

    public void showNoInternetDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(_context);
        alertDialog.setTitle("Gyddie Offline");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please check your internet connection.");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
    public void showRetrofitErrorToast(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK)
        GeneralFunctions.showShortToast(_context,"Check your internet connection");
        else
            GeneralFunctions.showShortToast(_context, "There might be some problem please try again later");
    }
    public void loginExpiredDialog() {
        mprefs.clearPrefs();
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(_context);
        alertDialog.setTitle("MedPrep");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Login Expired");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent=new Intent(_context, DashboardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        _context.startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}
