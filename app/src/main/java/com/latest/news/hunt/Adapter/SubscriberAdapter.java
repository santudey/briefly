package com.latest.news.hunt.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Fragment.SubscriptionFragment;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.ConnectionDetector;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.Subscribelist;
import com.latest.news.hunt.WebService.Rest_Client;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 16/5/16.
 */
public class SubscriberAdapter extends RecyclerView.Adapter< SubscriberAdapter.ViewHolder > {

    Context context;
    List< Subscribelist > subscriberList;
    boolean flagSubscribe = false;
    ConnectionDetector connectionDetector;
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator( 4 );
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;
    PrefsManager prefsManager;

       public SubscriberAdapter( Context context, List< Subscribelist > sublist ) {

        prefsManager = new PrefsManager( context );
        this.context = context;
        this.subscriberList = sublist;
        connectionDetector = new ConnectionDetector( context );
        fontRegular = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Regular.ttf");
        fontBold = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Bold.ttf");
        fontMedium = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Medium.ttf");
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        return new ViewHolder( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.subscriber_row, parent, false ) );
    }

    @Override
    public void onBindViewHolder( final ViewHolder holder, final int position ) {
        final Subscribelist listItem = subscriberList.get( position );
        if(position == getItemCount()-1) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            int marginSixteendp = ( int ) context.getResources().getDimension( R.dimen.sixteen_dp );
            int marginZerodp = ( int ) context.getResources().getDimension( R.dimen.zero_dp );
            params.setMargins(marginSixteendp, marginZerodp, marginSixteendp, marginZerodp);
            holder.cardSubscribe.setLayoutParams(params);
        }
        holder.tvMerchantName.setText( listItem.getPublisher_name( ) );
        Uri merchantLogoUri = Uri.parse( listItem.getPublisher_logo( ) );
        holder.dvMerchantLogo.setImageURI( merchantLogoUri );
        flagSubscribe = false;
        holder.tvNoOfSubscriber.setText(listItem.getNo_of_people_subscribe() );
       if(listItem.getSubscribed_by_me().equals( "false" )) {
           holder.tvNoOfSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.blue_border_without_left ) );
           holder.tvSubscriber.setTextColor( context.getResources( ).getColor( R.color.cpb_blue ) );
           holder.tvSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.border_blue ) );
           holder.tvSubscriber.setText( context.getResources().getString( R.string.subscribe ) );
       } else {
           holder.tvNoOfSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.grey_border_without_left ) );
           holder.tvSubscriber.setTextColor( context.getResources( ).getColor( R.color.cpb_grey_dark_light_ ) );
           holder.tvSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.border_grey ) );
           holder.tvSubscriber.setText( context.getResources().getString( R.string.subscribed ) );
       }
        holder.rlNoOfSubscriber.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {

                AppEventsLogger.activateApp( context );
                FirebaseAnalytics sublistClick = FirebaseAnalytics.getInstance( context );
                Bundle bundlsublist = new Bundle( );
                sublistClick.logEvent( "feed Subcribelist Click", bundlsublist );

                AnimatorSet animatorSet = new AnimatorSet( );
                ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat( holder.rlNoOfSubscriber, "scaleX", 0.2f, 1f );
                bounceAnimX.setDuration( 300 );
                bounceAnimX.setInterpolator( OVERSHOOT_INTERPOLATOR );

                ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat( holder.rlNoOfSubscriber, "scaleY", 0.2f, 1f );
                bounceAnimY.setDuration( 300 );
                bounceAnimY.setInterpolator( OVERSHOOT_INTERPOLATOR );
                if( flagSubscribe == false ) {
                    flagSubscribe = true;
                    if( listItem.getSubscribed_by_me( ).equals( "false" ) ) {
                        listItem.setSubscribed_by_me( "true" );
                        holder.tvNoOfSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.blue_border_without_left ) );
                        holder.tvSubscriber.setTextColor( context.getResources( ).getColor( R.color.cpb_blue ) );
                        holder.tvSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.border_blue ) );
                        holder.tvSubscriber.setText( context.getResources().getString( R.string.subscribe ) );
                    } else {
                        listItem.setSubscribed_by_me( "false" );
                        holder.tvNoOfSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.grey_border_without_left ) );
                        holder.tvSubscriber.setTextColor( context.getResources( ).getColor( R.color.cpb_grey_dark_light_ ) );
                        holder.tvSubscriber.setBackground( ContextCompat.getDrawable( context, R.drawable.border_grey ) );
                        holder.tvSubscriber.setText( context.getResources().getString( R.string.subscribed ) );
                    }
                    doSubscribe( position );
                }
            }
        } );
    }

    @Override
    public int getItemCount( ) {
        return subscriberList.size( );
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlNoOfSubscriber;
        TextView tvNoOfSubscriber;
        TextView tvMerchantName;
        TextView tvSubscriber;
        SimpleDraweeView dvMerchantLogo;
        CardView cardSubscribe;

        public ViewHolder( View itemView ) {
            super( itemView );
            tvNoOfSubscriber = ( TextView ) itemView.findViewById( R.id.tvNoOfSubscriber );
            tvMerchantName = ( TextView ) itemView.findViewById( R.id.tvMerchantName );
            tvSubscriber = ( TextView ) itemView.findViewById( R.id.tvSubscriber );
            dvMerchantLogo = ( SimpleDraweeView ) itemView.findViewById( R.id.dvMerchantLogo );
            rlNoOfSubscriber = ( RelativeLayout ) itemView.findViewById( R.id.rlNoOfSubscriber );
            cardSubscribe = ( CardView ) itemView.findViewById( R.id.cardSubscribe );
            tvNoOfSubscriber.setTypeface( fontRegular );
            tvMerchantName.setTypeface( fontRegular );
            tvSubscriber.setTypeface( fontRegular );
        }
    }

    private void doSubscribe( final int pos ) {
        final Subscribelist item = subscriberList.get( pos );
        try {
            Rest_Client.get( ).doSubscribe( prefsManager.getAccessToken(), subscriberList.get( pos ).getPublisher_name( ), new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    if( mainPojo.getStatus( ).equals( "Success" ) ) {
                        if( mainPojo.getResponse( ).equals( "Sucessfully subscribed" ) && item.getSubscribed_by_me( ).equals( "false" ) ) {
                            item.setSubscribed_by_me( "true" );
                        } else if( mainPojo.getResponse( ).equals( "Sucessfully Unsubscribe" ) && item.getSubscribed_by_me( ).equals( "true" ) ) {
                            item.setSubscribed_by_me( "false" );
                        }
                    } else {
                        if( item.getSubscribed_by_me( ).equals( "false" ) ) {
                            item.setSubscribed_by_me( "true" );
                        } else {
                            item.setSubscribed_by_me( "false" );
                        }
                    }
                    notifyDataSetChanged( );
                }

                @Override
                public void failure( RetrofitError error ) {
                    flagSubscribe = false;
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }
}
