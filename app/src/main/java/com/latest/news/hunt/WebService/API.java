package com.latest.news.hunt.WebService;

import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.RegisterPojo;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface API {

    @FormUrlEncoded
    @POST( "/new10/2/feed/" )
    void getFeed( @Field( "no" ) int no, @Field( "start" ) int start, @Field( "fnt" ) String fnt,
                  @Field( "userid" ) String users,Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/news/alreadysavednews" )
    void getSavedFeed( @Field( "no" ) int no, @Field( "start" ) int start, @Field( "userid" ) String userid, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/news/love" )
    void doLike( @Field( "userid" ) String userid, @Field( "newslink" ) String newslink, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/news/save" )
    void doSave( @Field( "userid" ) String userid, @Field( "newslink" ) String newslink, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/subscribe/" )
    void doSubscribe( @Field( "userid" ) String userid, @Field( "pubname" ) String pubname, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/pubprofile/" )
    void getPublisherProfile( @Field( "userid" ) String userid, @Field( "Publisher_name" ) String Publisher_name, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/pubfeed/" )
    void getPublisherFeed( @Field( "userid" ) String userid, @Field( "start" ) int start, @Field( "no" ) int no, @Field( "Publisher_name" ) String Publisher_name, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/detailnews/" )
    void getNewsDetails( @Field( "userid" ) String userid, @Field( "newslink" ) String newslink, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/relatednews/" )
    void getRelatedNews( @Field( "userid" ) String userid, @Field( "newslink" ) String newslink, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/mysubscribe/" )
    void getSubscribeList( @Field( "userid" ) String userid, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/allsubscribe/" )
    void getAllSubscribeList( @Field( "userid" ) String userid, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/trendingnews/" )
    void getTrendingnews( @Field( "userid" ) String userid,
                          @Field( "start" ) int start, @Field( "no" ) int no, Callback< MainPojo > callback );
    @FormUrlEncoded
    @POST( "/new10/2/publisherlist/" )
    void getSubscribeListbyCategory( @Field( "userid" ) String userid, Callback< MainPojo > callback );

    @FormUrlEncoded
    @POST( "/new10/2/addnewuser/" )
    void userRegister( @Field( "email" ) String email,
                       @Field( "appname" ) String appname,
                       @Field( "usercity" ) String usercity,
                       @Field( "deviceid" ) String deviceid,
                       Callback< MainPojo > callback );
}

