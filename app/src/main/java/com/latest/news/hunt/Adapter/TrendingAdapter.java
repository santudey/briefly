package com.latest.news.hunt.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Activity.NewsDetailActivity;
import com.latest.news.hunt.Activity.PublisherDetailsActivity;
import com.latest.news.hunt.Fragment.FeedFragment;
import com.latest.news.hunt.Fragment.TrendingFragment;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.CommonUtility;
import com.latest.news.hunt.Util.ConnectionDetector;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.Result;
import com.latest.news.hunt.WebService.Rest_Client;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 30/5/16.
 */
public class TrendingAdapter extends RecyclerView.Adapter< RecyclerView.ViewHolder > {

    List< Result > feedList;
    private Context context;
    private static final int ITEM_VIEW_TYPE_NORMAL = 0;
    private static final int ITEM_VIEW_TYPE_FOOTER = 1;
    ConnectionDetector connectionDetector;
    boolean flagLike = false;
    boolean flagSave = false;
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator( 4 );
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;
    PrefsManager prefsManager;

    public TrendingAdapter( List< Result > feedList, Context context ) {
        this.feedList = feedList;
        this.context = context;
        connectionDetector = new ConnectionDetector( context );
        fontRegular = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Regular.ttf");
        fontBold = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Bold.ttf");
        fontMedium = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Medium.ttf");
        prefsManager = new PrefsManager( context );
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        if( viewType == ITEM_VIEW_TYPE_NORMAL )
            return new TrendingAdapter.ViewHolderNoraml( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.feed_row_normal, parent, false ) );
        else
            return new TrendingAdapter.ViewHolderFooter( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.footer_progress, parent, false ) );
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, final int position ) {
        LinearLayoutManager layoutManager;
        SubscriberAdapter adapter;
        if( getItemViewType( position ) == ITEM_VIEW_TYPE_NORMAL ) {
            final ViewHolderNoraml viewHolderNoraml = ( ViewHolderNoraml ) holder;
            viewHolderNoraml.rlSubscription.setVisibility( View.GONE );
            viewHolderNoraml.cardMainView.setVisibility( View.GONE );
            viewHolderNoraml.rlSubsAd.setVisibility( View.GONE );
            viewHolderNoraml.tvPoint1.setVisibility( View.GONE );
            viewHolderNoraml.firstViewCirclePoint.setVisibility( View.GONE );
            viewHolderNoraml.tvPoint2.setVisibility( View.GONE );
            viewHolderNoraml.secondViewCirclePoint.setVisibility( View.GONE );
            viewHolderNoraml.tvPoint3.setVisibility( View.GONE );
            viewHolderNoraml.thirdViewCirclePoint.setVisibility( View.GONE );
            viewHolderNoraml.tvPoint4.setVisibility( View.GONE );
            viewHolderNoraml.fourthViewCirclePoint.setVisibility( View.GONE );
            viewHolderNoraml.tvPoint5.setVisibility( View.GONE );
            viewHolderNoraml.fifthViewCirclePoint.setVisibility( View.GONE );
            viewHolderNoraml.tvPoint1.setText( "" );
            viewHolderNoraml.tvPoint2.setText( "" );
            viewHolderNoraml.tvPoint3.setText( "" );
            viewHolderNoraml.tvPoint4.setText( "" );
            viewHolderNoraml.tvPoint5.setText( "" );
            String type = feedList.get( position ).getType( );
            if( type.equals( "1" ) ) {
                viewHolderNoraml.cardMainView.setVisibility( View.VISIBLE );
                viewHolderNoraml.tvHeadline.setText( feedList.get( position ).getTitle( ) );
                viewHolderNoraml.tvTime.setText( feedList.get( position ).getNewsPubdate( ) );
                viewHolderNoraml.tvMerchantName.setText( feedList.get( position ).getNews_publisher( ) );
                Uri CoverimageUri = Uri.parse( feedList.get( position ).getNews_feature_image( ) );
                viewHolderNoraml.dvPhoto.setImageURI( CoverimageUri );
                Uri merchantLogoUri = Uri.parse( feedList.get( position ).getNews_publisher_logo( ) );
                RoundingParams roundingParams = RoundingParams.fromCornersRadius( 5f );
                roundingParams.setBorder( Color.parseColor( "#ffffff" ), 1.0f );
                roundingParams.setRoundAsCircle( true );
                roundingParams.setOverlayColor( Color.parseColor( "#ffffff" ) );
                viewHolderNoraml.dvMerchantLogo.getHierarchy( ).setRoundingParams( roundingParams );
                viewHolderNoraml.dvMerchantLogo.setImageURI( merchantLogoUri );
                if( feedList.get( position ).getNpoint( ).size( ) > 0 ) {
                    if( feedList.get( position ).getNpoint( ).get( 0 ).getNpoint1( ).equals( "" ) ) {
                        viewHolderNoraml.tvPoint1.setVisibility( View.GONE );
                        viewHolderNoraml.firstViewCirclePoint.setVisibility( View.GONE );
                    } else {
                        viewHolderNoraml.tvPoint1.setVisibility( View.VISIBLE );
                        viewHolderNoraml.firstViewCirclePoint.setVisibility( View.VISIBLE );
                        viewHolderNoraml.tvPoint1.setText( feedList.get( position ).getNpoint( ).get( 0 ).getNpoint1( ) );
                    }
                    if( feedList.get( position ).getNpoint( ).get( 1 ).getNpoint2( ).equals( "" ) ) {
                        viewHolderNoraml.tvPoint2.setVisibility( View.GONE );
                        viewHolderNoraml.secondViewCirclePoint.setVisibility( View.GONE );
                    } else {
                        viewHolderNoraml.tvPoint2.setVisibility( View.VISIBLE );
                        viewHolderNoraml.secondViewCirclePoint.setVisibility( View.VISIBLE );
                        viewHolderNoraml.tvPoint2.setText( feedList.get( position ).getNpoint( ).get( 1 ).getNpoint2( ) );
                    }
                    if( feedList.get( position ).getNpoint( ).get( 2 ).getNpoint3( ).equals( "" ) ) {
                        viewHolderNoraml.tvPoint3.setVisibility( View.GONE );
                        viewHolderNoraml.thirdViewCirclePoint.setVisibility( View.GONE );
                    } else {
                        viewHolderNoraml.tvPoint3.setVisibility( View.VISIBLE );
                        viewHolderNoraml.thirdViewCirclePoint.setVisibility( View.VISIBLE );
                        viewHolderNoraml.tvPoint3.setText( feedList.get( position ).getNpoint( ).get( 2 ).getNpoint3( ) );
                    }
                    if( feedList.get( position ).getNpoint( ).get( 3 ).getNpoint4( ).equals( "" ) ) {
                        viewHolderNoraml.tvPoint4.setVisibility( View.GONE );
                        viewHolderNoraml.fourthViewCirclePoint.setVisibility( View.GONE );
                    } else {
                        viewHolderNoraml.tvPoint4.setVisibility( View.VISIBLE );
                        viewHolderNoraml.fourthViewCirclePoint.setVisibility( View.VISIBLE );
                        viewHolderNoraml.tvPoint4.setText( feedList.get( position ).getNpoint( ).get( 3 ).getNpoint4( ) );
                    }
                    if( feedList.get( position ).getNpoint( ).get( 4 ).getNpoint5( ).equals( "" ) ) {
                        viewHolderNoraml.tvPoint5.setVisibility( View.GONE );
                        viewHolderNoraml.fifthViewCirclePoint.setVisibility( View.GONE );
                    } else {
                        viewHolderNoraml.tvPoint5.setVisibility( View.VISIBLE );
                        viewHolderNoraml.fifthViewCirclePoint.setVisibility( View.VISIBLE );
                        viewHolderNoraml.tvPoint5.setText( feedList.get( position ).getNpoint( ).get( 4 ).getNpoint5( ) );
                    }
                }
                flagLike = false;
                flagSave = false;
                viewHolderNoraml.tvLikeCount.setText( String.valueOf( feedList.get( position ).getNews_love_count( ) ) );
                viewHolderNoraml.tvShareCount.setText( String.valueOf( feedList.get( position ).getNews_share_count( ) ) );
                viewHolderNoraml.tvSaveCount.setText( String.valueOf( feedList.get( position ).getNews_save_count() ) );
                if( feedList.get( position ).getLoved_by_me( ).equals( "false" ) ) {
                    viewHolderNoraml.ivLike.setImageResource( R.drawable.ic_favorite_border_blue );
                } else {
                    viewHolderNoraml.ivLike.setImageResource( R.drawable.ic_favorite_blue );
                }
                if( feedList.get( position ).getSaved_by_me( ).equals( "false" ) ) {
                    viewHolderNoraml.ivSave.setImageResource( R.drawable.ic_bookmark_border_black );
                } else {
                    viewHolderNoraml.ivSave.setImageResource( R.drawable.ic_bookmark_black );
                }

                viewHolderNoraml.rlMerchantDetails.setOnClickListener( new View.OnClickListener( ) {
                    @Override
                    public void onClick( View v ) {

                        AppEventsLogger.activateApp( context );
                        FirebaseAnalytics sublistClick = FirebaseAnalytics.getInstance( context );
                        Bundle bundlsublist = new Bundle( );
                        sublistClick.logEvent( "Trend Pub Details Click", bundlsublist );

                        Intent intent = new Intent( context, PublisherDetailsActivity.class );
                        intent.putExtra( "publisherName", feedList.get( position ).getNews_publisher() );
                        context.startActivity( intent );
                    }
                } );

                viewHolderNoraml.rlShare.setOnClickListener( new View.OnClickListener( ) {
                    @Override
                    public void onClick( View v ) {

                        AppEventsLogger.activateApp( context );
                        FirebaseAnalytics ShareClick = FirebaseAnalytics.getInstance( context );
                        Bundle bundlShare = new Bundle( );
                        ShareClick.logEvent( "Trend Share Click", bundlShare );

                        CommonUtility.shareinSocialApp(context,feedList.get( position ).getTitle( ) +
                                " for more download : https://play.google.com/store/apps/details?id=com.latest.news.hunt");
                    }
                } );

                viewHolderNoraml.rlLike.setOnClickListener( new View.OnClickListener( ) {
                    @Override
                    public void onClick( View v ) {
                        AppEventsLogger.activateApp( context );
                        FirebaseAnalytics likeClick = FirebaseAnalytics.getInstance( context );
                        Bundle bundllike = new Bundle( );
                        likeClick.logEvent( "Trend like Click", bundllike );

                        AnimatorSet animatorSet = new AnimatorSet( );
                        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat( viewHolderNoraml.ivLike, "scaleX", 0.2f, 1f );
                        bounceAnimX.setDuration( 300 );
                        bounceAnimX.setInterpolator( OVERSHOOT_INTERPOLATOR );

                        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat( viewHolderNoraml.ivLike, "scaleY", 0.2f, 1f );
                        bounceAnimY.setDuration( 300 );
                        bounceAnimY.setInterpolator( OVERSHOOT_INTERPOLATOR );
                        if( flagLike == false ) {
                            flagLike = true;
                            if( feedList.get( position ).getLoved_by_me( ).equals( "false" ) ) {
                                feedList.get( position ).setLoved_by_me( "true" );
                                feedList.get( position ).setNews_love_count( String.valueOf( Integer.parseInt( feedList.get( position ).getNews_love_count( ) ) + 1 ) );
                                viewHolderNoraml.tvLikeCount.setText( String.valueOf( feedList.get( position ).getNews_love_count( ) ) );
                                bounceAnimY.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationStart( Animator animation ) {
                                        viewHolderNoraml.ivLike.setImageResource( R.drawable.ic_favorite_border_blue );
                                    }
                                } );

                                animatorSet.play( bounceAnimX ).with( bounceAnimY );

                                animatorSet.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationEnd( Animator animation ) {
                                        viewHolderNoraml.ivLike.setImageResource( R.drawable.ic_favorite_blue );
                                    }
                                } );

                                animatorSet.start( );
                            } else {
                                feedList.get( position ).setLoved_by_me( "false" );
                                feedList.get( position ).setNews_love_count( String.valueOf( Integer.parseInt( feedList.get( position ).getNews_love_count( ) ) - 1 ) );
                                viewHolderNoraml.tvLikeCount.setText( String.valueOf( feedList.get( position ).getNews_love_count( ) ) );
                                bounceAnimY.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationStart( Animator animation ) {
                                        viewHolderNoraml.ivLike.setImageResource( R.drawable.ic_favorite_blue );
                                    }
                                } );

                                animatorSet.play( bounceAnimX ).with( bounceAnimY );

                                animatorSet.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationEnd( Animator animation ) {
                                        viewHolderNoraml.ivLike.setImageResource( R.drawable.ic_favorite_border_blue );
                                    }
                                } );

                                animatorSet.start( );
                            }
                            doLike( position );
                        }
                    }
                } );

                viewHolderNoraml.rlShave.setOnClickListener( new View.OnClickListener( ) {
                    @Override
                    public void onClick( View v ) {
                        AppEventsLogger.activateApp( context );
                        FirebaseAnalytics saveClick = FirebaseAnalytics.getInstance( context );
                        Bundle bundlsave = new Bundle( );
                        saveClick.logEvent( "Trend save Click", bundlsave );

                        AnimatorSet animatorSet = new AnimatorSet( );
                        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat( viewHolderNoraml.ivSave, "scaleX", 0.2f, 1f );
                        bounceAnimX.setDuration( 300 );
                        bounceAnimX.setInterpolator( OVERSHOOT_INTERPOLATOR );

                        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat( viewHolderNoraml.ivSave, "scaleY", 0.2f, 1f );
                        bounceAnimY.setDuration( 300 );
                        bounceAnimY.setInterpolator( OVERSHOOT_INTERPOLATOR );
                        if( flagSave == false ) {
                            flagSave = true;
                            if( feedList.get( position ).getSaved_by_me( ).equals( "false" ) ) {
                                feedList.get( position ).setSaved_by_me( "true" );
                                feedList.get( position ).setNews_save_count( String.valueOf( Integer.parseInt( feedList.get( position ).getNews_save_count( ) ) + 1 ) );
                                viewHolderNoraml.tvSaveCount.setText( String.valueOf( feedList.get( position ).getNews_save_count( ) ) );
                                bounceAnimY.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationStart( Animator animation ) {
                                        viewHolderNoraml.ivSave.setImageResource( R.drawable.ic_bookmark_border_black );
                                    }
                                } );

                                animatorSet.play( bounceAnimX ).with( bounceAnimY );

                                animatorSet.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationEnd( Animator animation ) {
                                        viewHolderNoraml.ivSave.setImageResource( R.drawable.ic_bookmark_black );
                                    }
                                } );

                                animatorSet.start( );
                            } else {
                                feedList.get( position ).setSaved_by_me( "false" );
                                feedList.get( position ).setNews_save_count( String.valueOf( Integer.parseInt( feedList.get( position ).getNews_save_count( ) ) - 1 ) );
                                viewHolderNoraml.tvSaveCount.setText( String.valueOf( feedList.get( position ).getNews_save_count( ) ) );
                                bounceAnimY.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationStart( Animator animation ) {
                                        viewHolderNoraml.ivSave.setImageResource( R.drawable.ic_bookmark_black );
                                    }
                                } );

                                animatorSet.play( bounceAnimX ).with( bounceAnimY );

                                animatorSet.addListener( new AnimatorListenerAdapter( ) {
                                    @Override
                                    public void onAnimationEnd( Animator animation ) {
                                        viewHolderNoraml.ivSave.setImageResource( R.drawable.ic_bookmark_border_black );
                                    }
                                } );

                                animatorSet.start( );
                            }
                            doSave( position );
                        }
                    }
                } );
                viewHolderNoraml.itemView.setOnClickListener( new View.OnClickListener( ) {
                    @Override
                    public void onClick( View v ) {
                        AppEventsLogger.activateApp( context );
                        FirebaseAnalytics bsdtClick = FirebaseAnalytics.getInstance( context );
                        Bundle bundlbsdt = new Bundle( );
                        bsdtClick.logEvent( "Trend Ns Dt Click", bundlbsdt );

                        Intent intent = new Intent( context, NewsDetailActivity.class );
                        intent.putExtra( "url", feedList.get( position ).getLink( ) );
                        intent.putExtra( "news_feature_image", feedList.get( position ).getNews_feature_image( ) );
                        context.startActivity( intent );

                    }
                } );
            } else if( type.equals( "2" ) ) {
                viewHolderNoraml.rlSubscription.setVisibility( View.VISIBLE );
                layoutManager = new LinearLayoutManager( context, LinearLayoutManager.HORIZONTAL, false );
                viewHolderNoraml.subsCriberRecyclerview.setItemAnimator( new DefaultItemAnimator( ) );
                viewHolderNoraml.subsCriberRecyclerview.setLayoutManager( layoutManager );
                adapter = new SubscriberAdapter( context, feedList.get( position ).getSubscribelist( ) );
                viewHolderNoraml.subsCriberRecyclerview.setAdapter( adapter );
            } else {
                viewHolderNoraml.rlSubsAd.setVisibility( View.VISIBLE );
            }
        } else if( getItemViewType( position ) == ITEM_VIEW_TYPE_FOOTER ) {
            final ViewHolderFooter viewHolderFooter = ( ViewHolderFooter ) holder;
            if( TrendingFragment.isnomore ) {
                viewHolderFooter.textView.setVisibility( View.VISIBLE );
                viewHolderFooter.progressWheel.setVisibility( View.GONE );
                viewHolderFooter.textView.setText( "No More Trending News" );
            } else {
                if( feedList.size() == 0 ) {
                    viewHolderFooter.progressWheel.setVisibility( View.GONE );
                    viewHolderFooter.textView.setVisibility( View.GONE );
                } else {
                    viewHolderFooter.textView.setVisibility( View.GONE );
                    viewHolderFooter.progressWheel.setVisibility( View.VISIBLE );
                }
            }
        }
    }

    @Override
    public int getItemViewType( int position ) {
        if( position == getItemCount( ) - 1 ) {
            return ITEM_VIEW_TYPE_FOOTER;
        } else {
            return ITEM_VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount( ) {
        return feedList.size( ) + 1;
    }

    public static class ViewHolderNoraml extends RecyclerView.ViewHolder {
        TextView tvHeadline;
        TextView tvLikeCount;
        TextView tvShareCount;
        TextView tvSaveCount;
        TextView tvTime;
        TextView tvMerchantName;
        SimpleDraweeView dvPhoto;
        SimpleDraweeView dvMerchantLogo;
        ImageView ivLike;
        ImageView ivShare;
        ImageView ivSave;
        View firstViewCirclePoint;
        View secondViewCirclePoint;
        View thirdViewCirclePoint;
        View fourthViewCirclePoint;
        View fifthViewCirclePoint;
        TextView tvPoint1;
        TextView tvPoint2;
        TextView tvPoint3;
        TextView tvPoint4;
        TextView tvPoint5;
        RelativeLayout rlSubscription;
        RelativeLayout rlSubsAd;
        RelativeLayout rlNews;
        RelativeLayout rlShave;
        RelativeLayout rlLike;
        RelativeLayout rlShare;
        CardView cardMainView;
        RecyclerView subsCriberRecyclerview;
        RelativeLayout rlMerchantDetails;

        public ViewHolderNoraml( View itemView ) {
            super( itemView );
            tvHeadline = ( TextView ) itemView.findViewById( R.id.tvHeadline );
            tvLikeCount = ( TextView ) itemView.findViewById( R.id.tvLikeCount );
            tvShareCount = ( TextView ) itemView.findViewById( R.id.tvShareCount );
            tvSaveCount = ( TextView ) itemView.findViewById( R.id.tvSaveCount );
            tvTime = ( TextView ) itemView.findViewById( R.id.tvTime );
            tvMerchantName = ( TextView ) itemView.findViewById( R.id.tvMerchantName );
            tvPoint1 = ( TextView ) itemView.findViewById( R.id.tvPoint1 );
            tvPoint2 = ( TextView ) itemView.findViewById( R.id.tvPoint2 );
            tvPoint3 = ( TextView ) itemView.findViewById( R.id.tvPoint3 );
            tvPoint4 = ( TextView ) itemView.findViewById( R.id.tvPoint4 );
            tvPoint5 = ( TextView ) itemView.findViewById( R.id.tvPoint5 );
            dvPhoto = ( SimpleDraweeView ) itemView.findViewById( R.id.dvPhoto );
            dvMerchantLogo = ( SimpleDraweeView ) itemView.findViewById( R.id.dvMerchantLogo );
            ivLike = ( ImageView ) itemView.findViewById( R.id.ivLike );
            ivShare = ( ImageView ) itemView.findViewById( R.id.ivShare );
            ivSave = ( ImageView ) itemView.findViewById( R.id.ivSave );
            firstViewCirclePoint = itemView.findViewById( R.id.viewCirclePoint1 );
            secondViewCirclePoint = itemView.findViewById( R.id.viewCirclePoint2 );
            thirdViewCirclePoint = itemView.findViewById( R.id.viewCirclePoint3 );
            fourthViewCirclePoint = itemView.findViewById( R.id.viewCirclePoint4 );
            fifthViewCirclePoint = itemView.findViewById( R.id.viewCirclePoint5 );
            rlNews = ( RelativeLayout ) itemView.findViewById( R.id.rlNews );
            rlSubsAd = ( RelativeLayout ) itemView.findViewById( R.id.rlSubsAd );
            rlSubscription = ( RelativeLayout ) itemView.findViewById( R.id.rlSubscription );
            rlShave = ( RelativeLayout ) itemView.findViewById( R.id.rlShave );
            rlLike = ( RelativeLayout ) itemView.findViewById( R.id.rlLike );
            rlShare = ( RelativeLayout ) itemView.findViewById( R.id.rlShare );
            cardMainView = ( CardView ) itemView.findViewById( R.id.cardMainView );
            subsCriberRecyclerview = ( RecyclerView ) itemView.findViewById( R.id.subsCriberRecyclerview );
            rlMerchantDetails = ( RelativeLayout ) itemView.findViewById( R.id.rlMerchantDetails );
            tvHeadline.setTypeface( fontMedium );
            tvLikeCount.setTypeface( fontRegular );
            tvShareCount.setTypeface( fontRegular );
            tvSaveCount.setTypeface( fontRegular );
            tvTime.setTypeface( fontRegular );
            tvMerchantName.setTypeface( fontRegular );
            tvPoint1.setTypeface( fontRegular );
            tvPoint2.setTypeface( fontRegular );
            tvPoint3.setTypeface( fontRegular );
            tvPoint4.setTypeface( fontRegular );
            tvPoint5.setTypeface( fontRegular );
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {
        ProgressWheel progressWheel;
        TextView textView;

        public ViewHolderFooter( View itemView ) {
            super( itemView );
            progressWheel = ( ProgressWheel ) itemView.findViewById( R.id.progress_bar );
            textView = ( TextView ) itemView.findViewById( R.id.tvNoData );
        }
    }

    private void doLike( final int pos ) {
        try {
            Rest_Client.get( ).doLike( prefsManager.getAccessToken(), feedList.get( pos ).getLink( ), new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    if( mainPojo.getStatus( ).equals( "Success" ) ) {
                        if( mainPojo.getResponse( ).equals( "Sucessfully loved" ) && feedList.get( pos ).getLoved_by_me( ).equals( "false" ) ) {
                            feedList.get( pos ).setLoved_by_me( "true" );
                        } else if( mainPojo.getResponse( ).equals( "Sucessfully remove loved" ) && feedList.get( pos ).getLoved_by_me( ).equals( "true" ) ) {
                            feedList.get( pos ).setLoved_by_me( "false" );
                        }
                    } else {
                        if( feedList.get( pos ).getLoved_by_me( ).equals( "false" ) ) {
                            feedList.get( pos ).setLoved_by_me( "true" );
                            feedList.get( pos ).setNews_love_count( String.valueOf( Integer.parseInt( feedList.get( pos ).getNews_love_count( ) ) + 1 ) );
                        } else {
                            feedList.get( pos ).setLoved_by_me( "false" );
                            feedList.get( pos ).setNews_love_count( String.valueOf( Integer.parseInt( feedList.get( pos ).getNews_love_count( ) ) - 1 ) );
                        }
                    }
                    notifyDataSetChanged( );
                }

                @Override
                public void failure( RetrofitError error ) {
                    flagLike = false;
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }

    private void doSave( final int pos ) {
        try {
            Rest_Client.get( ).doSave( prefsManager.getAccessToken(), feedList.get( pos ).getLink( ), new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    if( mainPojo.getStatus( ).equals( "Success" ) ) {
                        CommonUtility.isLiked = true;
                        if( mainPojo.getResponse( ).equals( "Sucessfully Saved" ) && feedList.get( pos ).getSaved_by_me( ).equals( "false" ) ) {
                            feedList.get( pos ).setSaved_by_me( "true" );
                        } else if( mainPojo.getResponse( ).equals( "Sucessfully remove saved" ) && feedList.get( pos ).getSaved_by_me( ).equals( "true" ) ) {
                            feedList.get( pos ).setSaved_by_me( "false" );
                        }
                    } else {
                        if( feedList.get( pos ).getSaved_by_me( ).equals( "false" ) ) {
                            feedList.get( pos ).setSaved_by_me( "true" );
                            feedList.get( pos ).setNews_save_count( String.valueOf( Integer.parseInt( feedList.get( pos ).getNews_save_count() ) + 1 ) );
                        } else {
                            feedList.get( pos ).setSaved_by_me( "false" );
                            feedList.get( pos ).setNews_save_count( String.valueOf( Integer.parseInt( feedList.get( pos ).getNews_save_count( ) ) - 1 ) );
                        }
                    }
                    notifyDataSetChanged( );
                }

                @Override
                public void failure( RetrofitError error ) {
                    flagSave = false;
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }
}

