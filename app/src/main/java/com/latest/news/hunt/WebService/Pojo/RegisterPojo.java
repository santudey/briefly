package com.latest.news.hunt.WebService.Pojo;

/**
 * Created by sushovan on 9/4/16.
 */
public class RegisterPojo {
    public int api_version;
    public String response = "";

    public int getApi_version( ) {
        return api_version;
    }

    public void setApi_version( int api_version ) {
        this.api_version = api_version;
    }

    public String getResponse( ) {
        return response;
    }

    public void setResponse( String response ) {
        this.response = response;
    }
}
