package com.latest.news.hunt.Util.AndroidRating;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;


import com.latest.news.hunt.R;

import java.util.Date;

public class AppRate {

    private static AppRate singleton;

    private final Context context;

    private final DialogOptions options = new DialogOptions();

    private int installDate = 10;

    private int launchTimes = 10;

    private int remindInterval = 1;

    private int eventsTimes = -1;

    private boolean isDebug = false;

    private AppRate(Context context) {
        this.context = context.getApplicationContext();
    }

    public static AppRate with(Context context) {
        if (singleton == null) {
            synchronized (AppRate.class) {
                if (singleton == null) {
                    singleton = new AppRate(context);
                }
            }
        }
        return singleton;
    }

    public AppRate setLaunchTimes(int launchTimes) {
        this.launchTimes = launchTimes;
        return this;
    }

    public AppRate setInstallDays(int installDate) {
        this.installDate = installDate;
        return this;
    }

    public AppRate setRemindInterval(int remindInterval) {
        this.remindInterval = remindInterval;
        return this;
    }

    public AppRate setShowLaterButton(boolean isShowNeutralButton) {
        options.setShowNeutralButton(isShowNeutralButton);
        return this;
    }

    public AppRate setEventsTimes(int eventsTimes) {
        this.eventsTimes = eventsTimes;
        return this;
    }

    public AppRate setShowTitle(boolean isShowTitle) {
        options.setShowTitle(isShowTitle);
        return this;
    }

    public AppRate clearAgreeShowDialog() {
        PreferenceHelper.setAgreeShowDialog(context, true);
        return this;
    }

    public AppRate clearSettingsParam() {
        PreferenceHelper.setAgreeShowDialog(context, true);
        PreferenceHelper.clearSharedPreferences(context);
        return this;
    }

    public AppRate setAgreeShowDialog(boolean clear) {
        PreferenceHelper.setAgreeShowDialog(context, clear);
        return this;
    }

    public AppRate setDebug(boolean isDebug) {
        this.isDebug = isDebug;
        return this;
    }

    public AppRate setView(View view) {
        options.setView(view);
        return this;
    }

    public AppRate setOnClickButtonListener(OnClickButtonListener listener) {
        options.setListener(listener);
        return this;
    }

    public AppRate setTitle(int resourceId) {
        options.setTitleResId(resourceId);
        return this;
    }

    public AppRate setMessage(int resourceId) {
        options.setMessageResId(resourceId);
        return this;
    }

    public AppRate setTextRateNow(int resourceId) {
        options.setTextPositiveResId(resourceId);
        return this;
    }

    public AppRate setTextLater(int resourceId) {
        options.setTextNeutralResId(resourceId);
        return this;
    }

    public AppRate setTextNever(int resourceId) {
        options.setTextNegativeResId(resourceId);
        return this;
    }

    public AppRate setCancelable(boolean cancelable) {
        options.setCancelable(cancelable);
        return this;
    }

    public void monitor() {
        if ( PreferenceHelper.isFirstLaunch(context)) {
            PreferenceHelper.setInstallDate(context);
        }
        PreferenceHelper.setLaunchTimes(context, PreferenceHelper.getLaunchTimes(context) + 1);
    }

    public static boolean showRateDialogIfMeetsConditions(Activity activity) {
        boolean isMeetsConditions = singleton.isDebug || singleton.shouldShowRateDialog();
        if (isMeetsConditions) {
            singleton.showRateDialog(activity);
        }
        return isMeetsConditions;
    }

    public static boolean passSignificantEvent(Activity activity) {
        return passSignificantEvent(activity, true);
    }

    public static boolean passSignificantEventAndConditions(Activity activity) {
        return passSignificantEvent(activity, singleton.shouldShowRateDialog());
    }

    private static boolean passSignificantEvent(Activity activity, boolean shouldShow) {
        int eventTimes = PreferenceHelper.getEventTimes(activity);
        PreferenceHelper.setEventTimes(activity, ++eventTimes);
        boolean isMeetsConditions = singleton.isDebug || (singleton.isOverEventPass() && shouldShow);
        if (isMeetsConditions) {
            singleton.showRateDialog(activity);
        }
        return isMeetsConditions;
    }

    @SuppressLint("NewApi")
    public void showRateDialog(Activity activity) {
        Typeface semiBoldMtf = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        Typeface boldMtf = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        if (!activity.isFinishing()) {
            Dialog rateDialog = DialogManager.create(activity, options);
            rateDialog.getWindow().getAttributes().windowAnimations = R.style.MyAnimation_Window;
            rateDialog.show();
            try {
                Button positiveButton = (Button) rateDialog.findViewById(android.R.id.button1);
                int sdk = android.os.Build.VERSION.SDK_INT;
                if( sdk < android.os.Build.VERSION_CODES.JELLY_BEAN ) {
                    positiveButton.setBackgroundDrawable( ContextCompat.getDrawable(context, R.drawable.rounded_corner));
                } else {
                    positiveButton.setBackground( ContextCompat.getDrawable(context, R.drawable.rounded_corner));
                }
                positiveButton.setTextSize(14f);
                positiveButton.setTypeface(boldMtf);
                positiveButton.setTextColor( ContextCompat.getColor(context, R.color.cpb_white));
                //b.setBackgroundColor(ContextCompat.getColor(context, R.color.action_bar_color));

                Button neutralButton = (Button) rateDialog.findViewById(android.R.id.button3);
                neutralButton.setTextSize(14f);
                neutralButton.setTypeface(semiBoldMtf);
                neutralButton.setTextColor( ContextCompat.getColor(context, R.color.cpb_grey));
            } catch( Exception e ) {}

        }
    }

    public boolean isOverEventPass() {
        return eventsTimes != -1 && PreferenceHelper.getEventTimes(context) > eventsTimes;
    }

    public boolean shouldShowRateDialog() {
        return PreferenceHelper.getIsAgreeShowDialog(context) &&
                isOverLaunchTimes() &&
                isOverInstallDate() &&
                isOverRemindDate();
    }

    private boolean isOverLaunchTimes() {
        return PreferenceHelper.getLaunchTimes(context) >= launchTimes;
    }

    private boolean isOverInstallDate() {
        return isOverDate(PreferenceHelper.getInstallDate(context), installDate);
    }

    private boolean isOverRemindDate() {
        return isOverDate(PreferenceHelper.getRemindInterval(context), remindInterval);
    }

    private static boolean isOverDate(long targetDate, int threshold) {
        return new Date().getTime() - targetDate >= threshold * 24 * 60 * 60 * 1000;
    }

    public boolean isDebug() {
        return isDebug;
    }

}
