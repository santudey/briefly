package com.latest.news.hunt.Util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by sushovan on 29/5/16.
 */
public class CommonUtility {
    public static boolean isLiked = false;
    public static boolean feedIsResumed = false;
    public static boolean savedIsResumed = false;
    public static boolean trendingIsResumed = false;
    public static boolean reRegistercall = false;
    public static void shareByWhatsApp( Context context, String title ) {

        PackageManager pm = context.getPackageManager( );
        try {
            Intent waIntent = new Intent( Intent.ACTION_SEND );
            waIntent.setType( "text/plain" );
            String text = title + "\nTo know more download " + "https://goo.gl/axy45q";
            PackageInfo info = pm.getPackageInfo( "com.whatsapp", PackageManager.GET_META_DATA );
            waIntent.setPackage( "com.whatsapp" );
            waIntent.putExtra( Intent.EXTRA_TEXT, text );
            context.startActivity( Intent.createChooser( waIntent, "Share with" ) );
        } catch( PackageManager.NameNotFoundException e ) {
            Toast.makeText( context, "WhatsApp not Installed", Toast.LENGTH_SHORT ).show( );
        }
    }

    public static void shareByFacebookMessanger( Context context, String title ) {
        Intent sendIntent = new Intent( );
        sendIntent.setAction( Intent.ACTION_SEND );
        sendIntent.putExtra( Intent.EXTRA_TEXT, title + "\nTo know more download " + "https://goo.gl/axy45q" );
        sendIntent.setType( "text/plain" );
        sendIntent.setPackage( "com.facebook.orca" );
        try {
            context.startActivity( sendIntent );
        } catch( android.content.ActivityNotFoundException ex ) {
            Toast.makeText( context, "Please Install Facebook Messenger", Toast.LENGTH_SHORT ).show( );
        }
    }

    public static void shareBygmail(Context context, String subject, String fromMail, String message, String toMail)
    {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setType("plain/text");
        sendIntent.setData(Uri.parse(fromMail));
        sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"branifyd@gmail.com"});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
       try {
           context.startActivity(sendIntent);
       } catch( Exception e ) {
           Toast.makeText( context, "Please Install Gmail", Toast.LENGTH_SHORT ).show( );
       }
    }

    public static void shareinSocialApp( Context context, String message) {
        Intent sendIntent = new Intent( );
        sendIntent.setAction( Intent.ACTION_SEND );
        sendIntent.putExtra( Intent.EXTRA_TEXT, message );
        sendIntent.setType( "text/plain" );
        context.startActivity( sendIntent );
    }

    public static String getEmailId( Context context ) {
        String possibleEmail = "";
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get( context ).getAccounts( );
        for( Account account : accounts ) {
            if( emailPattern.matcher( account.name ).matches( ) ) {
                possibleEmail = account.name;
            }
        }
        return possibleEmail;
    }

    public static String getCountry( Context context ) {
        String country_name = "";
        TelephonyManager tm;
        tm = ( TelephonyManager ) context.getSystemService( context.TELEPHONY_SERVICE );
        String countryCodeValue = tm.getNetworkCountryIso( );
        Locale loc = new Locale( "", countryCodeValue );
        country_name = loc.getDisplayCountry( ).replaceAll( "\\s+", "%20" );
        return countryCodeValue;
    }

    public static void shareByTwitter( Context context, String message) {
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_STREAM, message);
        List<ResolveInfo > resInfo = context.getPackageManager().queryIntentActivities(shareIntent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
            targetedShareIntent.setType("text/plain");
            targetedShareIntent.putExtra( Intent.EXTRA_TEXT, message);
            if( TextUtils.equals(resolveInfo.activityInfo.name,"com.twitter.android.composer.ComposerActivity")) {
                targetedShareIntent.setPackage(packageName);
                targetedShareIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                targetedShareIntents.add(targetedShareIntent);
            }
        }
        try {
            Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share With");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
            context.startActivity(chooserIntent);
        } catch( Exception e ) {
            Toast.makeText( context,"Please install Twitter",Toast.LENGTH_SHORT ).show();
        }
    }
}

