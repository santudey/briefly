package com.latest.news.hunt.WebService.Pojo;

/**
 * Created by sushovan on 15/5/16.
 */
public class Subscribelist {
    public String Publisher_logo;
    public String no_of_people_subscribe;
    public String publisher_name;
    public String subscribed_by_me = "false";

    public String getSubscribed_by_me( ) {
        return subscribed_by_me;
    }

    public void setSubscribed_by_me( String subscribed_by_me ) {
        this.subscribed_by_me = subscribed_by_me;
    }

    public String getPublisher_logo( ) {
        return Publisher_logo;
    }

    public void setPublisher_logo( String publisher_logo ) {
        Publisher_logo = publisher_logo;
    }

    public String getNo_of_people_subscribe( ) {
        return no_of_people_subscribe;
    }

    public void setNo_of_people_subscribe( String no_of_people_subscribe ) {
        this.no_of_people_subscribe = no_of_people_subscribe;
    }

    public String getPublisher_name( ) {
        return publisher_name;
    }

    public void setPublisher_name( String publisher_name ) {
        this.publisher_name = publisher_name;
    }
}
