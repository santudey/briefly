package com.latest.news.hunt.WebService.Pojo;

/**
 * Created by sushovan on 11/6/16.
 */
public class Category_item {
    public String Publisher_logo = "";
    public String subscribe_status = "";
    public String publisher_name = "";
    public String publisher_alias_name = "";
    public String publisher_feature_image = "";
    public String no_of_people_subscribe = "";

    public String getPublisher_logo( ) {
        return Publisher_logo;
    }

    public void setPublisher_logo( String publisher_logo ) {
        this.Publisher_logo = publisher_logo;
    }

    public String getSubscribe_status( ) {
        return subscribe_status;
    }

    public void setSubscribe_status( String subscribe_status ) {
        this.subscribe_status = subscribe_status;
    }

    public String getPublisher_name( ) {
        return publisher_name;
    }

    public void setPublisher_name( String publisher_name ) {
        this.publisher_name = publisher_name;
    }

    public String getPublisher_alias_name( ) {
        return publisher_alias_name;
    }

    public void setPublisher_alias_name( String publisher_alias_name ) {
        this.publisher_alias_name = publisher_alias_name;
    }

    public String getPublisher_feature_image( ) {
        return publisher_feature_image;
    }

    public void setPublisher_feature_image( String publisher_feature_image ) {
        this.publisher_feature_image = publisher_feature_image;
    }

    public String getNo_of_people_subscribe( ) {
        return no_of_people_subscribe;
    }

    public void setNo_of_people_subscribe( String no_of_people_subscribe ) {
        this.no_of_people_subscribe = no_of_people_subscribe;
    }
}
