package com.latest.news.hunt.WebService.Pojo;

import com.google.android.gms.common.api.Api;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sushovan on 2/4/16.
 */
public class Result {

    public String shared_by_me;
    public String isSubscribed = "1";

    public List<Category_item> category_item = new ArrayList<Category_item>();
    public String Category_name = "";

    public String header = "";
    public String news_alias_name = "";
    public Integer related_score = 0;

    public String news_html_text = "";
    public String news_publisher_alias_name = "";

    public String Publisher_logo = "";
    public String subscribe_status ="";
    public String publisher_name = "";
    public String publisher_alias_name = "";
    public String publisher_feature_image = "";
    public String no_of_people_subscribe = "";

    public String cover_image = "";
    public String publisher = "";
    public String news_source_logo = "";
    public String news_clean_text = "";
    public String date = "";

    public List<Npoint> npoint = new ArrayList<Npoint>();
    public String saved_by_me = "false";
    public String loved_by_me = "false";
    public String newsPubdate = "";
    public List<Subscribelist> subscribelist = new ArrayList<Subscribelist>();
    public String news_save_count = "";
    public String news_love_count = "";
    public String link = "";
    public String news_publisher = "";
    public String news_feature_image = "";
    public String title = "";
    public String news_publisher_logo = "";
    public String news_share_count = "";
    public String type = "1";

    public String getShared_by_me( ) {
        return shared_by_me;
    }

    public void setShared_by_me( String shared_by_me ) {
        this.shared_by_me = shared_by_me;
    }

    public String getIsSubscribed( ) {
        return isSubscribed;
    }

    public void setIsSubscribed( String isSubscribed ) {
        this.isSubscribed = isSubscribed;
    }

    public List< Category_item > getCategory_item( ) {
        return category_item;
    }

    public void setCategory_item( List< Category_item > category_item ) {
        this.category_item = category_item;
    }

    public String getCategory_name( ) {
        return Category_name;
    }

    public void setCategory_name( String category_name ) {
        Category_name = category_name;
    }

    public String getHeader( ) {
        return header;
    }

    public void setHeader( String header ) {
        this.header = header;
    }

    public String getNews_alias_name( ) {
        return news_alias_name;
    }

    public void setNews_alias_name( String news_alias_name ) {
        this.news_alias_name = news_alias_name;
    }

    public Integer getRelated_score( ) {
        return related_score;
    }

    public void setRelated_score( Integer related_score ) {
        this.related_score = related_score;
    }

    public String getNews_html_text( ) {
        return news_html_text;
    }

    public void setNews_html_text( String news_html_text ) {
        this.news_html_text = news_html_text;
    }

    public String getNews_publisher_alias_name( ) {
        return news_publisher_alias_name;
    }

    public void setNews_publisher_alias_name( String news_publisher_alias_name ) {
        this.news_publisher_alias_name = news_publisher_alias_name;
    }

    public String getPublisher_logo( ) {
        return Publisher_logo;
    }

    public void setPublisher_logo( String publisher_logo ) {
        Publisher_logo = publisher_logo;
    }

    public String getSubscribe_status( ) {
        return subscribe_status;
    }

    public void setSubscribe_status( String subscribe_status ) {
        this.subscribe_status = subscribe_status;
    }

    public String getPublisher_name( ) {
        return publisher_name;
    }

    public void setPublisher_name( String publisher_name ) {
        this.publisher_name = publisher_name;
    }

    public String getPublisher_alias_name( ) {
        return publisher_alias_name;
    }

    public void setPublisher_alias_name( String publisher_alias_name ) {
        this.publisher_alias_name = publisher_alias_name;
    }

    public String getPublisher_feature_image( ) {
        return publisher_feature_image;
    }

    public void setPublisher_feature_image( String publisher_feature_image ) {
        this.publisher_feature_image = publisher_feature_image;
    }

    public String getNo_of_people_subscribe( ) {
        return no_of_people_subscribe;
    }

    public void setNo_of_people_subscribe( String no_of_people_subscribe ) {
        this.no_of_people_subscribe = no_of_people_subscribe;
    }

    public String getCover_image( ) {
        return cover_image;
    }

    public void setCover_image( String cover_image ) {
        this.cover_image = cover_image;
    }

    public String getPublisher( ) {
        return publisher;
    }

    public void setPublisher( String publisher ) {
        this.publisher = publisher;
    }

    public String getNews_source_logo( ) {
        return news_source_logo;
    }

    public void setNews_source_logo( String news_source_logo ) {
        this.news_source_logo = news_source_logo;
    }

    public String getNews_clean_text( ) {
        return news_clean_text;
    }

    public void setNews_clean_text( String news_clean_text ) {
        this.news_clean_text = news_clean_text;
    }

    public String getDate( ) {
        return date;
    }

    public void setDate( String date ) {
        this.date = date;
    }

    public String getSaved_by_me( ) {
        return saved_by_me;
    }

    public void setSaved_by_me( String saved_by_me ) {
        this.saved_by_me = saved_by_me;
    }

    public String getLoved_by_me( ) {
        return loved_by_me;
    }

    public void setLoved_by_me( String loved_by_me ) {
        this.loved_by_me = loved_by_me;
    }

    public String getNewsPubdate( ) {
        return newsPubdate;
    }

    public void setNewsPubdate( String newsPubdate ) {
        this.newsPubdate = newsPubdate;
    }

    public List< Subscribelist > getSubscribelist( ) {
        return subscribelist;
    }

    public void setSubscribelist( List< Subscribelist > subscribelist ) {
        this.subscribelist = subscribelist;
    }

    public String getNews_save_count( ) {
        return news_save_count;
    }

    public void setNews_save_count( String news_save_count ) {
        this.news_save_count = news_save_count;
    }

    public String getNews_love_count( ) {
        return news_love_count;
    }

    public void setNews_love_count( String news_love_count ) {
        this.news_love_count = news_love_count;
    }

    public String getLink( ) {
        return link;
    }

    public void setLink( String link ) {
        this.link = link;
    }

    public String getNews_publisher( ) {
        return news_publisher;
    }

    public void setNews_publisher( String news_publisher ) {
        this.news_publisher = news_publisher;
    }

    public String getNews_feature_image( ) {
        return news_feature_image;
    }

    public void setNews_feature_image( String news_feature_image ) {
        this.news_feature_image = news_feature_image;
    }

    public String getTitle( ) {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public String getNews_publisher_logo( ) {
        return news_publisher_logo;
    }

    public void setNews_publisher_logo( String news_publisher_logo ) {
        this.news_publisher_logo = news_publisher_logo;
    }

    public String getNews_share_count( ) {
        return news_share_count;
    }

    public void setNews_share_count( String news_share_count ) {
        this.news_share_count = news_share_count;
    }

    public String getType( ) {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }

    public List< Npoint > getNpoint( ) {
        return npoint;
    }

    public void setNpoint( List< Npoint > npoint ) {
        this.npoint = npoint;
    }
}
