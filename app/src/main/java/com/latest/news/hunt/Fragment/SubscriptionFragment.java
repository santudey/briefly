package com.latest.news.hunt.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Adapter.SubscriptionDetailsAdapter;
import com.latest.news.hunt.R;
import com.latest.news.hunt.Util.CommonUtility;
import com.latest.news.hunt.Util.ConnectionDetector;
import com.latest.news.hunt.Util.GlobalInterface;
import com.latest.news.hunt.Util.PrefsManager;
import com.latest.news.hunt.WebService.Pojo.MainPojo;
import com.latest.news.hunt.WebService.Pojo.Result;
import com.latest.news.hunt.WebService.Rest_Client;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushovan on 3/5/16.
 */
public class SubscriptionFragment extends Fragment implements GlobalInterface {
    private RelativeLayout noDataOverlay;
    private ImageView ivInternet;
    private TextView noDataTitle;
    private TextView noDataSubTitle;
    private TextView noDataTv;
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;
    private RecyclerView mRecyclerview;
    LinearLayoutManager layoutManager;
    public List< Result > subscribList;
    SubscriptionDetailsAdapter adapter;
    ConnectionDetector connectionDetector;
    Context context;
    PrefsManager prefsManager;
    public static int scrollPosition = 0;
    public static boolean isLoad = true;
    ProgressWheel progress_bar;
    public static int mySubscriptionSize = 0;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        final View view = inflater.inflate( R.layout.fragment_subscription, container, false );
        context = getActivity( );
        return view;
    }

    @Override
    public void onActivityCreated( @Nullable Bundle savedInstanceState ) {
        super.onActivityCreated( savedInstanceState );
        if( context != null ) {
            AppEventsLogger.activateApp(getActivity());
            FirebaseAnalytics subscription = FirebaseAnalytics.getInstance(getActivity());
            Bundle bundlesubscription = new Bundle();
            subscription.logEvent("Feed Fragment", bundlesubscription);
            init( getView( ) );
            progress_bar.setVisibility( View.VISIBLE );
        }
        ivInternet.setOnClickListener( new View.OnClickListener( ) {
            @Override
            public void onClick( View v ) {
                isLoad = true;
                getMySubscribeList( );
            }
        } );
    }

    @Override
    public void onResume( ) {
        super.onResume( );
        isLoad = true;
        getMySubscribeList( );
    }

    private void init( View view ) {
        fontRegular = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Regular.ttf");
        fontBold = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Bold.ttf");
        fontMedium = Typeface.createFromAsset(context.getAssets( ),
                "fonts/Roboto-Medium.ttf");
        noDataOverlay = ( RelativeLayout ) view.findViewById( R.id.noDataOverlay );
        noDataTv = ( TextView ) view.findViewById( R.id.noDataTv );
        noDataTitle = ( TextView ) view.findViewById( R.id.noDataTitle );
        noDataSubTitle = ( TextView ) view.findViewById( R.id.noDataSubTitle );
        ivInternet = ( ImageView ) view.findViewById( R.id.ivInternet );
        noDataTv.setTypeface( fontRegular );
        noDataTitle.setTypeface( fontRegular );
        noDataSubTitle.setTypeface( fontRegular );
        noDataOverlay.setVisibility( View.GONE );
        prefsManager = new PrefsManager( context );
        subscribList = new ArrayList<>( );
        connectionDetector = new ConnectionDetector( context );
        progress_bar = ( ProgressWheel ) getView( ).findViewById( R.id.progress_bar );
        mRecyclerview = ( RecyclerView ) view.findViewById( R.id.mRecyclerview );
        layoutManager = new LinearLayoutManager( context );
        mRecyclerview.setLayoutManager( layoutManager );
    }

    private void getMySubscribeList( ) {
        try {
            Rest_Client.get( ).getSubscribeList( prefsManager.getAccessToken( ), new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    noDataOverlay.setVisibility( View.GONE );
                    mySubscriptionSize = mainPojo.getResults().size();
                    progress_bar.setVisibility( View.GONE );
                    subscribList.clear( );
                    Result subscribeItem = new Result( );
                    subscribeItem.setType( "2" );
                    subscribeItem.setHeader( "My Subscription" );
                    subscribeItem.setPublisher_logo( "" );
                    subscribeItem.setPublisher_name( "" );
                    subscribeItem.setPublisher_alias_name( "" );
                    subscribeItem.setPublisher_feature_image( "" );
                    subscribeItem.setNo_of_people_subscribe( "" );
                    subscribList.add( subscribeItem );
                    subscribList.addAll( mainPojo.getResults( ) );
                    adapter = new SubscriptionDetailsAdapter( subscribList, context, SubscriptionFragment.this );
                    mRecyclerview.setAdapter( adapter );
                    getAllSubscribeList( );
                }

                @Override
                public void failure( RetrofitError error ) {
                    noDataOverlay.setVisibility( View.VISIBLE );
                    progress_bar.setVisibility( View.GONE );
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }

    private void getAllSubscribeList( ) {
        try {
            Rest_Client.get( ).getSubscribeListbyCategory( prefsManager.getAccessToken( ), new Callback< MainPojo >( ) {
                @Override
                public void success( MainPojo mainPojo, Response response ) {
                    noDataOverlay.setVisibility( View.GONE );
                    isLoad = false;
                    for( int i = 0; i < mainPojo.getResults( ).size( ); i++ ) {
                        Result subscribeItem = new Result( );
                        subscribeItem.setType( "2" );
                        subscribeItem.setHeader( mainPojo.getResults( ).get( i ).getCategory_name( ) );
                        subscribeItem.setPublisher_logo( "" );
                        subscribeItem.setPublisher_name( "" );
                        subscribeItem.setPublisher_alias_name( "" );
                        subscribeItem.setPublisher_feature_image( "" );
                        subscribeItem.setNo_of_people_subscribe( "" );
                        subscribList.add( subscribeItem );
                        for( int j = 0; j < mainPojo.getResults( ).get( i ).getCategory_item( ).size( ); j++ ) {
                            Result subscribeItems = new Result( );
                            subscribeItems.setType( "1" );
                            subscribeItems.setHeader( "" );
                            subscribeItems.setPublisher_logo( mainPojo.getResults( ).get( i ).getCategory_item( ).get( j ).getPublisher_logo( ) );
                            subscribeItems.setPublisher_name( mainPojo.getResults( ).get( i ).getCategory_item( ).get( j ).getPublisher_name( ) );
                            subscribeItems.setPublisher_alias_name( mainPojo.getResults( ).get( i ).getCategory_item( ).get( j ).getPublisher_alias_name( ) );
                            subscribeItems.setPublisher_feature_image( mainPojo.getResults( ).get( i ).getCategory_item( ).get( j ).getPublisher_feature_image( ) );
                            subscribeItems.setNo_of_people_subscribe( mainPojo.getResults( ).get( i ).getCategory_item( ).get( j ).getNo_of_people_subscribe( ) );
                            subscribeItems.setSubscribe_status( mainPojo.getResults( ).get( i ).getCategory_item( ).get( j ).getSubscribe_status( ) );
                            subscribList.add( subscribeItems );
                        }
                    }
                    Result subscribeRequestItem = new Result( );
                    subscribeRequestItem.setType( "3" );
                    subscribeRequestItem.setHeader( "" );
                    subscribeRequestItem.setPublisher_logo( "" );
                    subscribeRequestItem.setPublisher_name( "" );
                    subscribeRequestItem.setPublisher_alias_name( "" );
                    subscribeRequestItem.setPublisher_feature_image( "" );
                    subscribeRequestItem.setNo_of_people_subscribe( "" );
                    subscribList.add( subscribeRequestItem );
                    adapter.notifyDataSetChanged( );
                    mRecyclerview.scrollToPosition( scrollPosition );
                }

                @Override
                public void failure( RetrofitError error ) {
                    noDataOverlay.setVisibility( View.VISIBLE );
                    connectionDetector.showRetrofitErrorToast( error );
                }
            } );
        } catch( NullPointerException e ) {
            e.printStackTrace( );
        }
    }

    public List< Result > getSubscribList( ) {
        return subscribList;
    }

    public void setSubscribList( List< Result > subscribList ) {
        this.subscribList = subscribList;
    }

    @Override
    public void getLoad( int val ) {
        if( val == 1 ) {
            isLoad = true;
            getMySubscribeList( );
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            CommonUtility.feedIsResumed = false;
            CommonUtility.trendingIsResumed = true;
        }
    }
}