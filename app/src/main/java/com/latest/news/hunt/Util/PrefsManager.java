package com.latest.news.hunt.Util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by SushovanDas on 8/9/15.
 */
public class PrefsManager {
    private static final String PREFS_NAME = "News_preferences";

    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private static final String PREFERENCES_REG_ID = "reg_id";
    private static final String PREFERENCES_REG_ID_DELIVERED = "isRegIdDelivered";
    private static final String PREFERENCES_APP_VERSION = "app_version";
    private static final String PREFERENCES_ACCESS_TOKEN = "access_token";

    public PrefsManager( Context context ) {
        mPreferences = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
    }

    public String getFirstInstall(){
        return mPreferences.getString ("first_install", "0");
    }
    public void setFirstInstall(String first_install)
    {
        mEditor.putString ("first_install",first_install);
        mEditor.commit ();
    }

    public String getApptUpdateCounter( ) {
        String appUpdateCounter = "";
        try {
            appUpdateCounter = mPreferences.getString( "appupdate_counter", "-1" );
        } catch( Exception e ) {
        }
        return appUpdateCounter;
    }

    public void setApptUpdateCounter( String update_counter ) {
        try {
            mEditor.putString( "appupdate_counter", update_counter );
            mEditor.commit( );
        } catch( Exception e ) {
        }
    }

    public String getGcmRegId() {
        return mPreferences.getString(PREFERENCES_REG_ID, "");
    }


    public void saveGcmRegId(String regId) {
        mEditor.putString(PREFERENCES_REG_ID, regId);
        mEditor.apply();
    }

    public boolean isRegIdDelivered() {
        return mPreferences.getBoolean(PREFERENCES_REG_ID_DELIVERED, false);
    }

    public void saveRegIdDeliveryReport(boolean flag) {
        mEditor.putBoolean(PREFERENCES_REG_ID_DELIVERED, flag);
        mEditor.apply();
    }

    public String getAccessToken() {
        String accessToken = "";
        try {
            accessToken = mPreferences.getString( PREFERENCES_ACCESS_TOKEN, "" );
        } catch( Exception e ) {
        }
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        try {
            mEditor.putString( PREFERENCES_ACCESS_TOKEN, accessToken );
            mEditor.commit( );
        } catch( Exception e ) {
        }
    }

    public String getcurrentAppVersion() {
        String currentAppVersion = "";
        try {
            currentAppVersion = mPreferences.getString( "currentAppVersion", "3" );
        } catch( Exception e ) {
        }
        return currentAppVersion;
    }

    public void setcurrentAppVersion(String currentAppVersion) {
        try {
            mEditor.putString( "currentAppVersion", currentAppVersion );
            mEditor.commit( );
        } catch( Exception e ) {
        }
    }

    public void saveAppVersion(int appVersion){
        mEditor.putInt(PREFERENCES_APP_VERSION, appVersion);
        mEditor.apply();
    }

    public int getAppVersion() {
        return mPreferences.getInt(PREFERENCES_APP_VERSION,0);
    }

    public void clearPrefs() {
        mEditor.clear();
        mEditor.apply();
    }
}
