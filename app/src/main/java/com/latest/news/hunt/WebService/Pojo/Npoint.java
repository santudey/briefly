package com.latest.news.hunt.WebService.Pojo;

/**
 * Created by sushovan on 15/5/16.
 */
public class Npoint {
    public String npoint1;
    public String npoint2;
    public String npoint3;
    public String npoint4;
    public String npoint5;

    public String getNpoint1( ) {
        return npoint1;
    }

    public void setNpoint1( String npoint1 ) {
        this.npoint1 = npoint1;
    }

    public String getNpoint2( ) {
        return npoint2;
    }

    public void setNpoint2( String npoint2 ) {
        this.npoint2 = npoint2;
    }

    public String getNpoint3( ) {
        return npoint3;
    }

    public void setNpoint3( String npoint3 ) {
        this.npoint3 = npoint3;
    }

    public String getNpoint4( ) {
        return npoint4;
    }

    public void setNpoint4( String npoint4 ) {
        this.npoint4 = npoint4;
    }

    public String getNpoint5( ) {
        return npoint5;
    }

    public void setNpoint5( String npoint5 ) {
        this.npoint5 = npoint5;
    }
}
