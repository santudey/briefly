package com.latest.news.hunt.WebService.Pojo;

/**
 * Created by sushovan on 3/4/16.
 */
public class SplashPojo {
    private String title;
    private String content;
    private String positive_button;

    public SplashPojo( String title, String content, String positive_button ) {
        this.title = title;
        this.content = content;
        this.positive_button = positive_button;
    }

    public SplashPojo( String title ) {
        this.title = title;
    }

    public String getTitle( ) {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public String getContent( ) {
        return content;
    }

    public void setContent( String content ) {
        this.content = content;
    }

    public String getPositive_button( ) {
        return positive_button;
    }

    public void setPositive_button( String positive_button ) {
        this.positive_button = positive_button;
    }
}
