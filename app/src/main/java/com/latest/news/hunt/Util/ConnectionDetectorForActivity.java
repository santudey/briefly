package com.latest.news.hunt.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import retrofit.RetrofitError;

/**
 * Created by codebrew on 20/10/15.
 */
public class ConnectionDetectorForActivity {
    private Activity _context;
    private PrefsManager mprefs;

    public ConnectionDetectorForActivity( Activity context ) {
        this._context = context;
        mprefs=new PrefsManager(context);
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null
                    && activeNetwork.isConnected();

            return isConnected;
        }
        return false;
    }

    public void showNoInternetDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(_context);
        alertDialog.setTitle("Gyddie Offline");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please check your internet connection.");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
    public void showRetrofitErrorToast(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK)
            GeneralFunctions.showShortToast(_context,"Check your internet connection");
        else
            GeneralFunctions.showShortToast(_context, "There might be some problem please try again later");
    }
}
