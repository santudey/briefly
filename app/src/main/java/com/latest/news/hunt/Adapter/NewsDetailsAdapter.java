package com.latest.news.hunt.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.latest.news.hunt.Activity.NewsDetailActivity;
import com.latest.news.hunt.Activity.PublisherDetailsActivity;
import com.latest.news.hunt.R;
import com.latest.news.hunt.WebService.Pojo.Result;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

/**
 * Created by sushovan on 29/5/16.
 */
public class NewsDetailsAdapter extends RecyclerView.Adapter< RecyclerView.ViewHolder > {
    private static final int ITEM_VIEW_TYPE_NORMAL = 0;
    private static final int ITEM_VIEW_TYPE_FOOTER = 1;
    private static final int ITEM_VIEW_TYPE_HEADER = 2;
    private static final int ITEM_VIEW_TYPE_ADS = 3;
    private static final int ITEM_VIEW_TYPE_PUBLISHERINFO_ROW = 4;
    private static final int ITEM_VIEW_TYPE_DEMO_ROW = 5;
    private static final int ITEM_VIEW_TYPE_FAKE_HEADERHIDESHOW_ROW = 6;
    List< Result > feedList;
    private Context context;
    String publishDate = "";
    String publisherLogo = "";
    String headLine = "";
    String htmlText = "";
    String publisherName = "";
    NewsDetailActivity activity;
    WebView wvNewsHtmlText;
    static Typeface fontRegular;
    static Typeface fontMedium;
    static Typeface fontBold;

    public NewsDetailsAdapter( List< Result > feedList, Context context, String publishDate, String publisherLogo, String headLine, String htmlText, String publisherName, NewsDetailActivity activity ) {
        this.feedList = feedList;
        this.context = context;
        this.publishDate = publishDate;
        this.publisherLogo = publisherLogo;
        this.headLine = headLine;
        this.htmlText = htmlText;
        this.publisherName = publisherName;
        this.activity = activity;
        fontRegular = Typeface.createFromAsset( context.getAssets( ), "fonts/Roboto-Regular.ttf" );
        fontBold = Typeface.createFromAsset( context.getAssets( ), "fonts/Roboto-Bold.ttf" );
        fontMedium = Typeface.createFromAsset( context.getAssets( ), "fonts/Roboto-Medium.ttf" );
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        RecyclerView.ViewHolder viewHolder = null;
        if( viewType == ITEM_VIEW_TYPE_NORMAL )

            viewHolder = new NewsDetailsAdapter.ViewHolderNoraml( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.news_details_normal_row, parent, false ) );
        else if( viewType == ITEM_VIEW_TYPE_HEADER )
            viewHolder = new NewsDetailsAdapter.ViewHolderHeader( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.news_details_header_row, parent, false ) );

//        else if( viewType == ITEM_VIEW_TYPE_FOOTER )
//
//            viewHolder = new NewsDetailsAdapter.ViewHolderFooter( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.news_details_footer_row, parent, false ) );
        else if( viewType == ITEM_VIEW_TYPE_ADS )

            viewHolder = new NewsDetailsAdapter.ViewHolderAds( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.news_details_ads_row, parent, false ) );
        else if( viewType == ITEM_VIEW_TYPE_PUBLISHERINFO_ROW )

            viewHolder = new NewsDetailsAdapter.ViewHolderPublisherInfo( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.news_details_publisherinfo_row, parent, false ) );
        else if( viewType == ITEM_VIEW_TYPE_DEMO_ROW )

            viewHolder = new NewsDetailsAdapter.ViewHolderDemoImage( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.demo_row, parent, false ) );
        else if( viewType == ITEM_VIEW_TYPE_FAKE_HEADERHIDESHOW_ROW )

            viewHolder = new NewsDetailsAdapter.ViewHolderFakeHeaderHideShow( LayoutInflater.from( parent.getContext( ) ).inflate( R.layout.newsdetails_fakerow_forhideshowheader, parent, false ) );

        return viewHolder;
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, final int position ) {
        if( getItemViewType( position ) == ITEM_VIEW_TYPE_NORMAL ) {
            final ViewHolderNoraml viewHolderNoraml = ( ViewHolderNoraml ) holder;
            int pos = position - 5;
            final Result item = feedList.get( pos );
            Uri newsFeatureimageoUri = Uri.parse( item.getNews_feature_image( ) );
            Uri publisherLogoUri = Uri.parse( item.getNews_publisher_logo( ) );
            viewHolderNoraml.sdvNewsfeatureImage.setImageURI( newsFeatureimageoUri );
            viewHolderNoraml.dvPublisherLogo.setImageURI( publisherLogoUri );
            viewHolderNoraml.tvHeadline.setText( item.getTitle( ) );
            viewHolderNoraml.tvPublisherName.setText( item.getNews_alias_name( ) );
            viewHolderNoraml.tvPublishDate.setText( item.getNewsPubdate( ) );
            viewHolderNoraml.itemView.setOnClickListener( new View.OnClickListener( ) {
                @Override
                public void onClick( View v ) {
                    AppEventsLogger.activateApp( context );
                    FirebaseAnalytics relatedClick = FirebaseAnalytics.getInstance( context );
                    Bundle bundlerelatedClick = new Bundle( );
                    relatedClick.logEvent( "Related News Click", bundlerelatedClick );

                    Intent intent = new Intent( context, NewsDetailActivity.class );
                    intent.putExtra( "url", item.getLink( ) );
                    intent.putExtra( "news_feature_image", item.getNews_feature_image( ) );
                    context.startActivity( intent );
                    ( ( Activity ) context ).finish( );
                }
            } );

        }
//        else if( getItemViewType( position ) == ITEM_VIEW_TYPE_FOOTER ) {
//            final ViewHolderFooter viewHolderFooter = ( ViewHolderFooter ) holder;
//        }
        else if( getItemViewType( position ) == ITEM_VIEW_TYPE_PUBLISHERINFO_ROW ) {
            final ViewHolderPublisherInfo viewHolderPublisherInfo = ( ViewHolderPublisherInfo ) holder;
            Uri merchantLogoUri = Uri.parse( publisherLogo );
            viewHolderPublisherInfo.dvMerchantLogo.setImageURI( merchantLogoUri );
            viewHolderPublisherInfo.tvHeadline.setText( headLine );
            viewHolderPublisherInfo.tvPublishDate.setText( publishDate );
            viewHolderPublisherInfo.rlPublisherInfo.setOnClickListener( new View.OnClickListener( ) {
                @Override
                public void onClick( View v ) {

                    AppEventsLogger.activateApp( context );
                    FirebaseAnalytics pubDetClick = FirebaseAnalytics.getInstance( context );
                    Bundle bundlepubDetClick = new Bundle( );
                    pubDetClick.logEvent( "News Dtails Publi Info Clck", bundlepubDetClick );

                    Intent intent = new Intent( context, PublisherDetailsActivity.class );
                    intent.putExtra( "publisherName", publisherName );
                    context.startActivity( intent );
                    ( ( Activity ) context ).finish( );
                }
            } );
        } else if( getItemViewType( position ) == ITEM_VIEW_TYPE_HEADER ) {
            final ViewHolderHeader viewHolderHeader = ( ViewHolderHeader ) holder;
            WebSettings settings = wvNewsHtmlText.getSettings( );
            settings.setJavaScriptEnabled( true );
            settings.setDomStorageEnabled( true );
            settings.setUserAgentString( "Mozilla/5.0 (Linux; U;Android 2.0; en-us; Droid Build/ESD20) " + "AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17" );
//            wvNewsHtmlText.getSettings().setBuiltInZoomControls(true);
//            wvNewsHtmlText.setInitialScale(100);
//            wvNewsHtmlText.getSettings().setUseWideViewPort(true);
            String htmlwithcss = "<html><head><style>body { padding-left: 10px;padding-right: 10px;}</style></head><body>" + htmlText + "</body></html>";
            wvNewsHtmlText.loadDataWithBaseURL( "fonts/Roboto-Regular.ttf", htmlwithcss, "text/html", "UTF-8", "" );

        } else if( getItemViewType( position ) == ITEM_VIEW_TYPE_DEMO_ROW ) {
            final ViewHolderDemoImage viewHolderDemoImage = ( ViewHolderDemoImage ) holder;
        } else if( getItemViewType( position ) == ITEM_VIEW_TYPE_FAKE_HEADERHIDESHOW_ROW ) {
            final ViewHolderFakeHeaderHideShow viewHolderFakeHeaderHideShow = ( ViewHolderFakeHeaderHideShow ) holder;
        } else {
            final ViewHolderAds viewHolderAds = ( ViewHolderAds ) holder;
            viewHolderAds.itemView.setOnClickListener( new View.OnClickListener( ) {
                @Override
                public void onClick( View v ) {

                    AppEventsLogger.activateApp( context );
                    FirebaseAnalytics adsClick = FirebaseAnalytics.getInstance( context );
                    Bundle bundleadsClick = new Bundle( );
                    adsClick.logEvent( "News Dtails Ads Clck", bundleadsClick );
                }
            } );
        }
    }

    @Override
    public int getItemCount( ) {
        return feedList.size( ) + 5;
    }

    @Override
    public int getItemViewType( int position ) {
        if( position == 0 ) {
            return ITEM_VIEW_TYPE_FAKE_HEADERHIDESHOW_ROW;
        } else if( position == 1 ) {
            return ITEM_VIEW_TYPE_DEMO_ROW;
        } else if( position == 2 ) {
            return ITEM_VIEW_TYPE_PUBLISHERINFO_ROW;
        } else if( position == 3 ) {
            return ITEM_VIEW_TYPE_HEADER;
        } else if( position == 4 ) {
            return ITEM_VIEW_TYPE_ADS;
        }
//        else if( position == getItemCount( ) - 1 ) {
//            return ITEM_VIEW_TYPE_FOOTER;
//        }
        else {
            return ITEM_VIEW_TYPE_NORMAL;
        }
    }

    public static class ViewHolderNoraml extends RecyclerView.ViewHolder {
        SimpleDraweeView sdvNewsfeatureImage;
        SimpleDraweeView dvPublisherLogo;
        TextView tvHeadline;
        TextView tvPublisherName;
        TextView tvPublishDate;

        public ViewHolderNoraml( View itemView ) {
            super( itemView );
            dvPublisherLogo = ( SimpleDraweeView ) itemView.findViewById( R.id.dvPublisherLogo );
            sdvNewsfeatureImage = ( SimpleDraweeView ) itemView.findViewById( R.id.sdvNewsfeatureImage );
            tvHeadline = ( TextView ) itemView.findViewById( R.id.tvHeadline );
            tvPublisherName = ( TextView ) itemView.findViewById( R.id.tvPublisherName );
            tvPublishDate = ( TextView ) itemView.findViewById( R.id.tvPublishDate );
            tvHeadline.setTypeface( fontMedium );
            tvPublisherName.setTypeface( fontRegular );
            tvPublishDate.setTypeface( fontRegular );
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {
        TextView tvNoMore;

        public ViewHolderFooter( View itemView ) {
            super( itemView );
            tvNoMore = ( TextView ) itemView.findViewById( R.id.tvNoMore );
            tvNoMore.setTypeface( fontRegular );
        }
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder {
        public ViewHolderHeader( View itemView ) {
            super( itemView );
            wvNewsHtmlText = ( WebView ) itemView.findViewById( R.id.wvNewsHtmlText );
        }
    }

    public class ViewHolderAds extends RecyclerView.ViewHolder {
        TextView tvRelatedNews;
        RelativeLayout rlAd;
        NativeExpressAdView adView;

        public ViewHolderAds( View itemView ) {
            super( itemView );
            tvRelatedNews = ( TextView ) itemView.findViewById( R.id.tvRelatedNews );
            rlAd = ( RelativeLayout ) itemView.findViewById( R.id.rlAd );
            adView = ( NativeExpressAdView ) itemView.findViewById( R.id.adView );
            tvRelatedNews.setTypeface( fontBold );
            AdRequest adRequest = new AdRequest.Builder( ).addTestDevice( AdRequest.DEVICE_ID_EMULATOR ).build( );
            adView.loadAd( adRequest );
        }
    }

    public class ViewHolderPublisherInfo extends RecyclerView.ViewHolder {
        SimpleDraweeView dvMerchantLogo;
        TextView tvPublishDate;
        TextView tvHeadline;
        RelativeLayout rlPublisherInfo;

        public ViewHolderPublisherInfo( View itemView ) {
            super( itemView );
            dvMerchantLogo = ( SimpleDraweeView ) itemView.findViewById( R.id.dvMerchantLogo );
            tvPublishDate = ( TextView ) itemView.findViewById( R.id.tvPublishDate );
            tvHeadline = ( TextView ) itemView.findViewById( R.id.tvHeadline );
            rlPublisherInfo = ( RelativeLayout ) itemView.findViewById( R.id.rlPublisherInfo );
            tvPublishDate.setTypeface( fontRegular );
            tvHeadline.setTypeface( fontBold );
        }
    }

    public class ViewHolderDemoImage extends RecyclerView.ViewHolder {
        LinearLayout rlImage;

        public ViewHolderDemoImage( View itemView ) {
            super( itemView );
            rlImage = ( LinearLayout ) itemView.findViewById( R.id.rlDemo );
        }
    }

    public class ViewHolderFakeHeaderHideShow extends RecyclerView.ViewHolder {
        LinearLayout lnrFake;

        public ViewHolderFakeHeaderHideShow( View itemView ) {
            super( itemView );
            lnrFake = ( LinearLayout ) itemView.findViewById( R.id.lnrFake );
        }
    }
}
